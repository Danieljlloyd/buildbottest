#!/bin/bash

# To install script, add it to root's crontab:
#     $ sudo crontab -e
# Add this to run the script every hour:
#     0 * * * * /path/to/internet_check.sh


# Test for internet connectivity
/bin/ping -c1 www.google.com >/dev/null

# If the test failed, restart the device in case it's a driver/hardware problem
if [ "$?" != 0 ]; then
    echo "$(/bin/date): No internet! Restarting now." >> /var/log/internet_check.log
    /sbin/shutdown -r now
else
    : # Uncomment to check if the cron job is running
    #echo "$(/bin/date): Internet is fine, didn't restart" >> /var/log/internet_check.log
fi

exit

