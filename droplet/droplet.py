#!/usr/bin/python
import paho.mqtt.client as paho
import os
import sys
import time
import uuid
import netifaces
import threading
import platform
import json
import logging
from datetime import datetime
from fractions import gcd
from collections import OrderedDict
from SimpleXMLRPCServer import SimpleXMLRPCServer, \
                               SimpleXMLRPCRequestHandler

from Measurements_pb2 import MeasurementSnapshot, Measurement, Quality
from Commands_pb2 import CommandRequest
from devices import *
from controllers import *
from aggregators import *

from stateStore import stateStore
from dropletAPI import DropletAPIThread 

BLAST_VERSION = "Droplet 0.3"

def on_disconnect(mosq, userdata, rc):
    # userdata is the droplet instance
    userdata.send_log_message(logging.INFO, "Disconnected from the server")

def on_subscribe(mosq, obj, mid, qos_list):
  print("Subscribe with mid "+str(mid)+" received.")

def on_unsubscribe(mosq, obj, mid):
  print("Unsubscribe with mid "+str(mid)+" received.")

class Droplet:
  def __init__(self, identifier, host, conf_file, printing):
        # Capture any exceptions so that we still connect to the broker and
        # are ready to receive commands
        try:
            self.printing = printing
            self.devices = []
            self.controllers = []
            self.aggregators = []

            self.uuid_str = identifier

            self.config = {}
            self._load_default_config()
            self._load_config_file(conf_file)

            self.stateStore = stateStore()

            for device in self.config.get('devices', []):
                try:
                    self.devices.append(base.DeviceFactory().newDevice(device['identifier'], device))
                    print 'Loaded device named "{0}" of type "{1}"...'.format(device['name'], device['identifier'])
                except Exception as ex:
                    print "Couldn't load a device: " + str(ex)

            for aggregator in self.config.get('aggregators', []):
                try:
                    self.aggregators.append(aggregatorBase.AggregatorFactory().newAggregator(aggregator['identifier'], self.config))
                    print 'Loaded aggregator named "{0}" of type "{1}"...'.format(aggregator['name'], aggregator['identifier'])
                except Exception as ex:
                    print "Couldn't load an aggregator: " + str(ex)

            for controller in self.config.get('controllers', []):
                try:
                    self.controllers.append(controllerBase.ControllerFactory().newController(controller['identifier'], self.config))
                    print 'Loaded controller named "{0}" of type "{1}"...'.format(controller['name'], controller['identifier'])
                except Exception as ex:
                    print "Couldn't load a controller: " + str(ex)


            if len(self.devices) == 0:
                print 'Warning: No devices found to load!!!'

        except Exception as e:
            print 'Problem initialising: ' + str(e)

        try:
            self.config['mqtt_broker_host'] = host

            self.mqtt_connection = None
            self.connect_to_broker()
        except Exception as e:
            print 'Problem connecting to MQTT broker: ' + str(e)
            raise

        # Start the droplet API server
        if self.config.get('rpcserver', False):
            myDropletAPIThread = DropletAPIThread(self.stateStore, self.config['rpcserver'])
            myDropletAPIThread.start()

  def _load_default_config(self):
    self.config['mqtt_broker_host'] = None
    self.config['mqtt_broker_port'] = 1883
    self.config['mqtt_heartbeat_period'] = 60   # seconds
    self.config['mqtt_reconnect_delay'] = 60    # seconds
    self.config['measurement_period'] = 1       # second
    self.config['controller_period'] = 1        # second
    self.config['ping_period'] = 5              # seconds
    self.config['info_period'] = 60 * 10        # seconds
    self.config['reverse_ssh_host'] = 'sshpi.switchd.in'
    self.config['reverse_ssh_user'] = 'sshpi'


  def _load_config_file(self, conf_file):
    try:
      with open(conf_file) as json_file:
        json_data = json.load(json_file, object_pairs_hook=OrderedDict)
        try:
          self.config.update(json_data)
        except ValueError:
          print "Error parsing JSON in config file. Please fix and rerun the program."
          sys.exit()
    except IOError:
      print "Config file {0} not found.".format(conf_file)


  def _mqtt_loop(self):
    """Process the incoming mqtt messages, and handle reconnects if the connection
    is dropped.

    This is a blocking call, so this function should be spawned in a thread."""

    self.mqtt_connection.loop_forever(1, 5) # Seconds to wait, max packets to process

  def connect_to_broker(self):
    pid = os.getpid()
    unique_client_id = "droplet" + str(pid)# + "_" + self.uuid_str

    self.mqtt_connection = paho.Client(unique_client_id, userdata=self)
    #self.mqtt_connection.reconnect_delay_set(1, self.config['mqtt_reconnect_delay'], True)

    self.mqtt_connection.on_message = Droplet.on_message
    self.mqtt_connection.on_connect = Droplet.on_connect
    self.mqtt_connection.on_disonnect = on_disconnect
    self.mqtt_connection.on_subscribe = on_subscribe
    self.mqtt_connection.on_unsubscribe = on_unsubscribe

    self.mqtt_connection.connect(
        self.config['mqtt_broker_host'],
        self.config['mqtt_broker_port'],
        self.config['mqtt_heartbeat_period'],
        "")

  @staticmethod
  def on_connect(mosq, self, rc):
    mosq.subscribe("devices/{0}/#".format(self.uuid_str), 0)

    self.send_log_message(logging.INFO, "Connected to the server")

  @staticmethod
  def on_message(mosq, self, msg):
    #print("Message received on topic "+msg.topic+" with QoS "+str(msg.qos)+" and payload "+msg.payload)
    try:
      if msg.topic.endswith("/command"):
        self.handle_command(msg)
      elif msg.topic.endswith("/pong"):
        self.handle_pong(msg)
      elif msg.topic.endswith("/json_command"):
        self.handle_json_command(msg)
      else:
        self.send_log_message(logging.WARN, "Unknown command type {0}".format(msg.topic))
    except Exception as e:
      self.send_log_message(logging.ERROR, e.message)

  def send_log_message(self, level, message, device_id=None, extra=None):
    print "Log: " + message

    message_body = {
            "severity": level,
            "message": message,
            "extra": extra,
            "device_id": device_id,
            "endpoint_uuid": self.uuid_str
            }

    topic = "measurements"

    snapshot = MeasurementSnapshot()
    measurement = snapshot.measurements.add()

    measurement.point_uuid.value = self.uuid_str
    measurement.name = "{0}.SWDINPlant.LLN0.DRCS[ST]".format(self.uuid_str)
    measurement.type = Measurement.STRING
    measurement.string_val = json.dumps(message_body)
    measurement.quality.validity = Quality.GOOD
    measurement.time = int(self.current_unix_time())
    measurement.is_device_time = True

    msg = snapshot.SerializeToString()

#    self.mqtt_connection.publish(topic, bytearray(msg))

  def handle_command(self, msg):
    cmd_request = CommandRequest()
    cmd_request.ParseFromString(msg.payload)

    if cmd_request.command.endpoint.uuid.value == self.uuid_str:
      # Message is for this device
      command_name = cmd_request.command.name
      if command_name in ("BATTERY ON", "BATTERY OFF", "SET MODE"):
        for device in self.devices:
          device.command(command_name)

      print "Received command."

  def upgrade(self):
    self.send_log_message(logging.INFO, "Performing self upgrade")

    import subprocess
    subprocess.call(['./upgrade.sh'])

  def current_unix_time(self):
    now = datetime.now()
    return time.mktime(now.timetuple()) + (now.microsecond / 1e6)

  def handle_pong(self, msg):
    snapshot = MeasurementSnapshot()
    snapshot.ParseFromString(msg.payload)

    # Add received time
    measurement = snapshot.measurements.add()
    measurement.point_uuid.value = self.uuid_str
    measurement.name = "time:stop"
    measurement.type = Measurement.DOUBLE
    measurement.double_val = self.current_unix_time()
    measurement.quality.validity = Quality.GOOD
    measurement.time = int(self.current_unix_time())
    measurement.is_device_time = True

    # Post back to be logged
    topic = "measurements"
    msg = snapshot.SerializeToString()
#    self.mqtt_connection.publish(topic, bytearray(msg))

    print "Received Pong, sent measurement."


  def handle_json_command(self, msg):
    data = json.loads(msg.payload)
    command_name = data.get("command", "")
    command_data = data.get("data", None)

    if command_name == "CONFIGURE":
      # Update existing config with new values
      if command_data is not None:
        self.config.update(command_data)

    elif command_name == "REVERSE SSH ON":
      if "port" in command_data:
        self.start_reverse_ssh(command_data["port"])

    elif command_name == "UPGRADE":
      self.upgrade()

    else:
      for device in self.devices:
        handled = device.command(command_name, command_data)
        if handled:
          break
      else:
        self.send_log_message(logging.ERROR, "Unknown JSON command: {0}".format(command_name))

  def start_reverse_ssh(self, remote_port):
    self.send_log_message(logging.DEBUG, "Opening reverse SSH on port {0}".format(remote_port))

    command = "/usr/bin/ssh -R {0}:localhost:22 -n -N -t -o StrictHostKeyChecking=no {1}@{2}".format(
          remote_port,
          self.config['reverse_ssh_user'],
          self.config['reverse_ssh_host'])

    import subprocess
    subprocess.Popen(command.split(" "))

  def post_measurements(self):
    """ Send all measurements to the message broker. """
    for device in self.devices:
      try:
        measures = device.measurement_values()
        self._post_snapshot(measures)
      except Exception as e:
        self.send_log_message(logging.ERROR, e.message)

    print "Posted measurements."

  def post_device_settings(self):
    for device in self.devices:
      try:
        settings = device.setting_values()
        self._post_snapshot(settings)
      except Exception as e:
        self.send_log_message(logging.ERROR, e.message)

  def _post_snapshot(self, measures):
    """ Post a single measurement to the message broker. """
    topic = "measurements"

    snapshot = MeasurementSnapshot()

    # Add time to the state store
    self.stateStore.putVal( 'time', datetime.now())

    for measure in measures:
      name = measure[0]
      value = measure[1]
      # Store Measure In Shared State Database
      self.stateStore.putVal( name, value )
      # Build Structure For Transmission
      measurement = snapshot.measurements.add()
      measurement.point_uuid.value = self.uuid_str
      if name.startswith('SWDIN'):
        measurement.name = '{0}.{1}'.format(self.uuid_str, name)
      else:
        measurement.name = name
      measurement.type = Measurement.DOUBLE
      measurement.double_val = value
      measurement.quality.validity = Quality.GOOD
      measurement.time = int(self.current_unix_time())
      measurement.is_device_time = True

      if self.printing:
        print "name: {0} value {1}".format(name, value)

    msg = snapshot.SerializeToString()

#    self.mqtt_connection.publish(topic, bytearray(msg))

    # Dump the state store
    print "---- State store"
    self.stateStore.printStateStore()

  def perform_aggregations(self):
    """ Perform Local Aggregations """

    # Run Each Controller and Perform Control Actions
    for aggregator in self.aggregators:
      try:
        aggregatedMeasures = aggregator.periodicProcessing()
        self._post_snapshot(aggregatedMeasures)
      except Exception as e:
        self.send_log_message(logging.ERROR, e.message)

    print "Posted Aggregations."


  def perform_control_actions(self):
    """ Run Controllers and Perform Control Actions """

    # Determine Control Actions
    actions = []
    # Run Each Controller and Perform Control Actions
    for controller in self.controllers:
      actions = controller.periodicProcessing()

      # Perform Control Actions
      for action in actions:
        for device in self.devices:
          #print "Action : " + str(action)
          handled = device.command("SETSWOOP", action)
          if handled:
            break

    print "Performed Control Actions."


  def generate_ping(self):
    # flush out waiting messages - to make ping as fast as possible
    self.mqtt_connection.loop(0)

    measures = [('time:start', self.current_unix_time())]
    self._post_snapshot(measures)

    print "Sent ping."

  def send_device_info(self):
    measures = [
        ('droplet_version', BLAST_VERSION),
        ('processor', platform.processor()),
        ('hostname', platform.node()),
        ('python_version', platform.python_version()),
        ('python_implementation', platform.python_implementation()),
        ('os_release', platform.release()),
        ('os_version', platform.version()),
    ]

    local_ip_info = self._local_ip_info()

    for key,value in local_ip_info.iteritems():
        measures.append((key,value))

    snapshot = MeasurementSnapshot()

    for measure in measures:
      measurement = snapshot.measurements.add()
      measurement.point_uuid.value = self.uuid_str
      measurement.name = measure[0]
      measurement.type = Measurement.STRING
      measurement.string_val = measure[1]
      measurement.quality.validity = Quality.GOOD
      measurement.time = int(self.current_unix_time())
      measurement.is_device_time = True

    topic = "measurements"
    msg = snapshot.SerializeToString()
#    self.mqtt_connection.publish(topic, bytearray(msg))

    print "Posted device info."

  def _local_ip_info(self):
    info = {}

    try:
        # Use the default gateway's info
        gateway = netifaces.gateways()['default'][netifaces.AF_INET]
        info['gateway_address'] = gateway[0]
        info['gateway_interface'] = gateway[1]

        interface_addresses = netifaces.ifaddresses(info['gateway_interface'])[netifaces.AF_INET][0]
        info['ip_address'] = interface_addresses['addr']
        info['ip_netmask'] = interface_addresses['netmask']
        info['ip_broadcast'] = interface_addresses['broadcast']

        # Legacy
        info['local_ip_address'] = info['ip_address']
    except Exception as e:
        self.send_log_message(logging.WARNING, "Couldn't get IP info: {0}".format(e.message))

    return info

def lcm(a, b):
    """Return lowest common multiple."""
    return a * b // gcd(a, b)

def main(args):
  """ Program entry point when run from the command line. """
  import sys
  import argparse

  parser = argparse.ArgumentParser(description='Simulate a simple solar installation.')
  parser.add_argument('-b', '--broker', default='localhost',
                     help='MQTT broker to connect to (default: localhost)')
  parser.add_argument('-d', '--device',
                     help='Device ID (default: generate UUID from MAC address)')
  parser.add_argument('-c', '--config', default='config.json',
                     help='A JSON config file to use (default config.json)')
  parser.add_argument('-p', '--print', dest='printing', action='store_const', const=1, default=0,
                     help='Print debugging messages. (default off)')

  _args = parser.parse_args()

  if _args.device:
      device_id = _args.device
  else:
      # First try to use MAC address of eth0
      if 'eth0' in netifaces.interfaces():
          mac_address = netifaces.ifaddresses('eth0')[netifaces.AF_PACKET][0]['addr']
          device_id = "00000000-0000-0000-0000-" + mac_address.replace(":", "")

      else:
          device_id = str(uuid.UUID(int=uuid.getnode()))

  mqtt_host = _args.broker
  conf_file = _args.config

  printing = _args.printing

  print "Device ID: {0}".format(device_id)
  print "Broker:    {0}".format(mqtt_host)

  droplet = Droplet(device_id, mqtt_host, conf_file, printing)

  loop_thread = threading.Thread(target=droplet._mqtt_loop)
  loop_thread.setDaemon(True)
  loop_thread.start()

  loop_count = 0
  while True:
    measurement_period = droplet.config['measurement_period']
    controller_period = droplet.config['controller_period']
    ping_period = droplet.config['ping_period']
    info_period = droplet.config['info_period']

    # Measurements
    if loop_count % measurement_period == 0:
      for device in droplet.devices:
        device.heartbeat_tick()
        droplet.post_measurements()

    # Aggregators
    if loop_count % measurement_period == 0:
      droplet.perform_aggregations()

    # Controllers
    if loop_count % controller_period == 0:
      droplet.perform_control_actions()

    # Pinging disabled for now
    #if loop_count % ping_period == 0:
    #  droplet.generate_ping()

    if loop_count % info_period == 0:
      droplet.send_device_info()
      droplet.post_device_settings()

    periods = [measurement_period, ping_period, info_period]
    reset_every = reduce(lcm, periods) # Lowest common multiple of periods
    if loop_count % reset_every == 0:
      loop_count = 1 # Stop overflow if we run forever
    else:
      loop_count += 1

    time.sleep(1)

if __name__ == '__main__':
  import sys
  main(sys.argv[1:])
