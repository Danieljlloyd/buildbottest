#!/usr/bin/env bash
source ~/venv/droplet/bin/activate
git pull

cd ..
pip install -r requirements.txt
cd droplet

sudo supervisorctl restart droplet
