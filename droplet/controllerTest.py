from controllers import *
from stateStore import stateStore

class testHarness:
    def __init__(self, testId, tickLength, battInit, chargeInc, chargeDec):
        self.stateStore = stateStore()
        self.controller = nccNumberTwoController.nccNumberTwoController({})
        self.time = 0
        self.tickLength = tickLength
        self.chargeInc = chargeInc
        self.chargeDec = chargeDec
        self.testId = testId

        self.stateStore.putVal('time', self.time)
        self.stateStore.putVal('SWDINPV.VIC.ZBAT1.Vol.instMag[MX]', battInit)

    def charge(self):
        charge = self.stateStore.getVal('SWDINPV.VIC.ZBAT1.Vol.instMag[MX]') + (self.chargeInc/100.0)
        if charge > 49.0:
            charge = 49.0
        self.stateStore.putVal('SWDINPV.VIC.ZBAT1.Vol.instMag[MX]', charge)

    def discharge(self):
        charge = self.stateStore.getVal('SWDINPV.VIC.ZBAT1.Vol.instMag[MX]') - (self.chargeDec / 100.0)
        self.stateStore.putVal('SWDINPV.VIC.ZBAT1.Vol.instMag[MX]', charge)

    def tick(self):
        time = self.stateStore.getVal('time') + self.tickLength

        if time >= 24:
            self.day=self.day+1
            time = 0

        self.stateStore.putVal('time', time)

    def publish(self):
        state     = self.stateStore.getVal('SWIDINPV.NccNo2Controller.State')
        time      = self.stateStore.getVal('time')
        vbatt     = self.stateStore.getVal('SWDINPV.VIC.ZBAT1.Vol.instMag[MX]')
        invMode   = self.stateStore.getVal('SWDINPV.VIC.Mode.instMag[MX]')
        invCurLim = self.stateStore.getVal('SWDINPV.VIC.InpCurLim.instMag[MX]')
        line = '-' * 50
        #print('{}\nState: {}\nTime: {}\nVbatt: {}\n'.format(line, state, time, vbatt))
        print('{},{},{},{},{},{}'.format(self.testId,(self.day * 24.0) + time,state,vbatt,invMode,invCurLim))

    def run(self):
        self.day=0
        for i in range(int(48.0/self.tickLength)):
            # Get the current state
            currState = self.stateStore.getVal('SWIDINPV.NccNo2Controller.State')

            # Charge/discharge based on current state
            self.tick()
            if(currState == "Grid Feed"):
                pass

            elif(currState == "Charge"):
                self.charge()

            elif(currState == "Discharge"):
                self.discharge()

            self.controller.periodicProcessing()
            self.performControlActions()
            self.publish()

    def performControlActions(self):
        actionList = self.stateStore.getVal('SWDINPV.ControllerActionList')

        for action in actionList:
            self.stateStore.putVal( action['metric'], action['value'])

def drange(start, stop, step):
    r = start
    while r < stop:
        yield r
        r += step

if __name__ == "__main__":
    print('{},{},{},{},{},{}'.format('TestId','time','contState','battV','invMode','ICL'))
    for vbattInit in drange(45, 47, 0.4):
        for chargeRate in drange(0.2, 2.0, 0.4):
            for dischargeRate in drange(0.2,2.0,0.4):
                testId = '{}-{}-{}'.format(vbattInit, chargeRate, dischargeRate)
                testHarness(testId, 0.01, vbattInit, chargeRate, dischargeRate).run()
