import mosquitto

from Measurements_pb2 import MeasurementSnapshot, Measurement, Quality
from Commands_pb2 import CommandRequest


def on_disconnect(mosq, userdata, rc):
    print "Disconnected"

def on_subscribe(mosq, obj, mid, qos_list):
    print("Subscribe with mid "+str(mid)+" received.")

def on_unsubscribe(mosq, obj, mid):
    print("Unsubscribe with mid "+str(mid)+" received.")

class Storm(object):

    def __init__(self, config, command_callback):

        self.uuid_str = None
        self.config = config
        self.command_callback = command_callback

        self.mqtt_connection = None
        self.loop_thread = None

    def connect(self):
        self.connect_to_broker() 
        
        # Start a background thread to process mqtt events
        self.loop_thread = threading.Thread(target=droplet._mqtt_loop)
        self.loop_thread.setDaemon(True)
        self.loop_thread.start()

    def disconnect(self):
        self.mqtt_connection.disconnect()

    def connect_to_broker(self):
        pid = os.getpid()
        unique_client_id = "drplt_" + str(pid)# + "_" + self.uuid_str

        reconnect_delay = self.config.get('mqtt_reconnect_delay', 60) # seconds
        broker_host = self.config.get('mqtt_broker_host', 'localhost')
        broker_port = self.config.get('mqtt_broker_port', 1883)
        heartbeat_period = self.config.get('mqtt_heartbeat_period', 60) # seconds

        self.mqtt_connection = mosquitto.Mosquitto(unique_client_id, userdata=self)
        self.mqtt_connection.reconnect_delay_set(1, reconnect_delay, True)
        self.mqtt_connection.connect(
                broker_host,
                broker_port,
                heartbeat_period,
                True)

        self.mqtt_connection.on_message = Storm.on_message
        self.mqtt_connection.on_connect = Storm.on_connect
        self.mqtt_connection.on_disonnect = on_disconnect
        self.mqtt_connection.on_subscribe = on_subscribe
        self.mqtt_connection.on_unsubscribe = on_unsubscribe

    def _mqtt_loop(self):
        """Process the incoming mqtt messages, and handle reconnects if the connection
        is dropped.
                   
        This is a blocking call, so this function should be spawned in a thread."""

        self.mqtt_connection.loop_forever(1, 5) # Seconds to wait, max packets to process

    @staticmethod
    def on_connect(mosq, self, rc):
        print "Connected"
        mosq.subscribe("devices/{0}/#".format(self.uuid_str), 0)

    @staticmethod
    def on_message(mosq, self, msg):
        #print("Message received on topic "+msg.topic+" with QoS "+str(msg.qos)+" and payload "+msg.payload)

        if msg.topic.endswith("/command"):
            self.handle_command(msg)
        elif msg.topic.endswith("/pong"):
            self.handle_pong(msg)
        elif msg.topic.endswith("/json_command"):
            self.handle_json_command(msg)
        else:
            print "Unknown command type {0}".format(msg.topic)




