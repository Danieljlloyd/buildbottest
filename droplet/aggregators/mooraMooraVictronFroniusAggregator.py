from aggregatorBase import BaseAggregator

#
# Use Case:
#
# Aggregators use the shared state store to combine or select metrics from specific
# devices and publish them as the site / unit level record for that metric.
#
# So if there are multiple devices measuring components of a
# site level measurement, then the aggregator should pull those together and
# publish the site level metric from the combination of them.
#

#
# Assumptions about connectivity:
# 1. Fronius Galvo Grid Tied Inverter Connects to Minigrid
# 2. Fronious is AC coupled to Victron Multiplex on the UPS input
# 3. Loads are connected to AC coupled Victron / Fronious Connection
# 4. Victron Multiplus is DC coupled to MPPT tracker
#

class mooraMooraVictronFroniusAggregator( BaseAggregator ):

   # Class Constructor
   def __init__( self, config={} ):
      super(mooraMooraVictronFroniusAggregator, self).__init__( )
      self.config = config

   # Identifier for Config File
   @staticmethod
   def implements(identifier):
      return "mooraMooraVictronFroniusAggregator" in identifier

   # Periodic Processing
   def periodicProcessing( self ):

      aggregatedMeasures = []

      # Site Generated Power - From Fronius Inverter (Need to add Victron MPPT Tracker when panels connected)
      self.pvProduction = self.stateStore.getVal('SWDINPV.FRO.MMXN3.TotW.instMag[MX]')
      self.stateStore.putVal( 'SWDINPV.MMXN1.TotW.instMag[MX]', self.pvProduction )
      aggregatedMeasures.append( ('SWDINPV.MMXN1.TotW.instMag[MX]', self.pvProduction) )

      # Battery Power = Power From Victron Multiplus (add Power from Victron MPPT when panels connected)
      self.batteryPower = -0.001 * self.stateStore.getVal('SWDINPV.VIC.ZBAT1.Vol.instMag[MX]') * self.stateStore.getVal('SWDINPV.VIC.ZBAT1.Cur.instMag[MX]')
      self.stateStore.putVal( 'SWDINPV.ZBAT.TotW.instMag[MX]', self.batteryPower )
      aggregatedMeasures.append( ('SWDINPV.ZBAT.TotW.instMag[MX]', self.batteryPower) )

      # Grid Is Connected To AC Input Of Victron
      self.gridPower = self.stateStore.getVal('SWDINPV.VIC.MMXN3.TotW.instMag[MX]')
      self.stateStore.putVal( 'SWDINPV.MMXN3.TotW.instMag[MX]', self.gridPower )
      aggregatedMeasures.append( ('SWDINPV.MMXN3.TotW.instMag[MX]', self.gridPower) )

      # Assume all production goes to loads until a power meter is avauilable
      # Load Power = Victron UPS1 Power + PV Power
      self.loadPower = self.stateStore.getVal('SWDINPV.VIC.MMXN2.TotW.instMag[MX]') + self.pvProduction - self.batteryPower
      self.loadPower = max( self.loadPower, 0.0 )
      self.stateStore.putVal( 'SWDINPV.MMXN2.TotW.instMag[MX]', self.loadPower )
      aggregatedMeasures.append( ('SWDINPV.MMXN2.TotW.instMag[MX]',self.loadPower) )

      # Battery SoC from Victron 
      self.batterySoC = self.stateStore.getVal('SWDINPV.VIC.ZBAT1.SoC.instMag[MX]')
      self.stateStore.putVal( 'SWDINPV.ZBAT.SoC.instMag[MX]', self.batterySoC )
      aggregatedMeasures.append( ('SWDINPV.ZBAT.SoC.instMag[MX]', self.batterySoC) )

      return aggregatedMeasures


