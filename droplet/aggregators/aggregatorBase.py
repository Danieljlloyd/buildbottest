from types import TypeType
from stateStore import stateStore

class BaseAggregator(object):
   def __init__(self, config={}) :
      self.stateStore = stateStore()
      self.config = config
      pass

   def __del__(self) :
      pass

   def periodicProcessing(self, state_store) :
      pass

   def implements( identifier ):
      return False

class AggregatorFactory(object):
    @staticmethod
    def newAggregator(identifier, config={}):
        # Walk through all Controller classes
        aggregatorClasses = BaseAggregator.__subclasses__()
        for aggregatorClass in aggregatorClasses:
            if aggregatorClass.implements(identifier):
                return aggregatorClass(config)
        # if research was unsuccessful, raise an error
        raise ValueError('No aggregator supporting "%s".' % identifier)
