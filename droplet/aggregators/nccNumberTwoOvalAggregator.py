from aggregatorBase import BaseAggregator

#
# Use Case:
#
# Aggregators use the shared state store to combine or select metrics from specific
# devices and publish them as the site / unit level record for that metric.
#
# So if there are multiple devices measuring components of a 
# site level measurement, then the aggregator should pull those together and
# publish the site level metric from the combination of them.
#

class nccNumberTwoOvalAggregator( BaseAggregator ):

   # Class Constructor
   def __init__( self, config={} ):
      super(nccNumberTwoOvalAggregator, self).__init__( )
      self.config = config

   # Identifier for Config File
   @staticmethod
   def implements(identifier):
      return "nccNumberTwoOvalAggregator" in identifier

   # Periodic Processing
   def periodicProcessing( self ):

      aggregatedMeasures = []

      # Site Generated Power - From the SMA Tripower Inverter
      self.smaPanelProduction = self.stateStore.getVal('SWDINPV.SMATP.MMXN1.TotW.instMag[MX]')
      self.stateStore.putVal( 'SWDINPV.MMXN1.TotW.instMag[MX]', self.smaPanelProduction )
      aggregatedMeasures.append( ('SWDINPV.MMXN1.TotW.instMag[MX]', self.smaPanelProduction) )

      # Site Load Power - From the Victron For Now ... will be power meter in final 
      self.victronLoad = self.stateStore.getVal('SWDINPV.VIC.MMXN2.TotW.instMag[MX]')
      self.stateStore.putVal( 'SWDINPV.MMXN2.TotW.instMag[MX]', self.victronLoad )
      aggregatedMeasures.append( ('SWDINPV.MMXN2.TotW.instMag[MX]', self.victronLoad) )

      # Site Exported Power - Net Grid Power
      self.victronBatCurr = self.stateStore.getVal('SWDINPV.VIC.ZBAT1.Cur.instMag[MX]')
      self.victronBatVolt = self.stateStore.getVal('SWDINPV.VIC.ZBAT1.Vol.instMag[MX]')
      self.batteryPower   = self.victronBatCurr * self.victronBatVolt

      # Note: Will get from power meter when it is installed
      # Exported Power = Generation - Battery - Load
      self.exportedPower = self.smaPanelProduction - self.victronLoad - self.batteryPower
      self.stateStore.putVal( 'SWDINPV.MMXN3.TotW.instMag[MX]', self.exportedPower )
      aggregatedMeasures.append( ('SWDINPV.MMXN3.TotW.instMag[MX]', self.exportedPower) )


      return aggregatedMeasures
