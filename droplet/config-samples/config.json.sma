{
   "devices": [
    {
      "identifier": "Modbus", 
      "name": "SMA Tripower Inverter",
      "modbus": {
        "type": "TCP", 
        "port": 502, 
        "ip": "192.168.8.68", 
        "measurements": [
        {
            "label": "SWDINPV.SMATP.MMXN1.Cur.instMag[MX]",
            "description": "DCCurrentInput", 
            "convertformat": "<i", "multiplier": 0.001, "reversebyteorder": 1, 
            "length": 2, "functioncode": 3, "address": 30769, "units": "A", "dataformat": ">BBBB",
            "slaveid": 3, "minvalue": 0, "maxvalue": 100
        }, 
        {
            "label": "SWDINPV.SMATP.MMXN1.Vol.instMag[MX]",
            "description": "DCVoltageInput", 
            "convertformat": "<i", "multiplier": 0.01, "reversebyteorder": 1, 
            "length": 2, "functioncode": 3, "address": 30771, "units": "V", "dataformat": ">BBBB", 
            "slaveid": 3, "minvalue": 0, "maxvalue": 1000
        }, 
        {
            "label": "SWDINPV.SMATP.MMXN1.TotW.instMag[MX]",
            "description": "DCPowerInput", 
            "convertformat": "<i", "multiplier": 0.001, "reversebyteorder": 1, "length": 2, 
            "functioncode": 3, "address": 30773, "units": "kW", "dataformat": ">BBBB", 
            "slaveid": 3, "minvalue": 0, "maxvalue": 100
        }, 
        {
            "label": "SWDINPV.SMATP.MMXN3.TotW.instMag[MX]",
            "description": "ACActivePower3P", 
            "convertformat": "<i", "multiplier": 0.001, "reversebyteorder": 1, "length": 2,
            "functioncode": 3, "address": 30775, "units": "kW", "dataformat": ">BBBB", 
            "slaveid": 3, "minvalue": 0, "maxvalue": 100
        }, 
        {
            "label": "SWDINPV.SMATP.MMXN3.Vol.instMag[MX]",
            "description": "GridVoltageAB", 
            "convertformat": "<I", "multiplier": 0.01, "reversebyteorder": 1, "length": 2,
            "functioncode": 3, "address": 30789, "units": "V", "dataformat": ">BBBB", 
            "slaveid": 3, "minvalue": 0, "maxvalue": 1000
        }, 
        {
            "label": "SWDINPV.SMATP.MMXN3.Cur.instMag[MX]",
            "description": "GridCurrent", 
            "convertformat": "<I", "multiplier": 0.001, "reversebyteorder": 1, "length": 2,
            "functioncode": 3, "address": 30795, "units": "A", "dataformat": ">BBBB", 
            "slaveid": 3, "minvalue": 0, "maxvalue": 100
        }, 
        {
            "label": "SWDINPV.SMATP.MMXN3.Freq.instMag[MX]",
            "description": "PowerFrequency", 
            "convertformat": "<I", "multiplier": 0.01, "reversebyteorder": 1, "length": 2,
            "functioncode": 3, "address": 30803, "units": "Hz", "dataformat": ">BBBB", 
            "slaveid": 3, "minvalue": 30, "maxvalue": 70
        }, 
        {
            "label": "SWDINPV.SMATP.MMXN3.TotW.instMag[MX]",
            "description": "GridReactivePower",
            "convertformat": "<i", "multiplier": 0.001, "reversebyteorder": 1, "length": 2,
            "functioncode": 3, "address": 30805, "units": "kVar", "dataformat": ">BBBB",
            "slaveid": 3, "minvalue": -10, "maxvalue": 10
        },
        {
            "label": "SWDINPV.SMATP.MMXN3.TotW.instMag[MX]",
            "description": "GridApparentPower",
            "convertformat": "<i", "multiplier": 0.001, "reversebyteorder": 1, "length": 2,
            "functioncode": 3, "address": 30813, "units": "kVA", "dataformat": ">BBBB",
            "slaveid": 3, "minvalue": 0, "maxvalue":100
        }
        ] 
      } 
    }
  ]
}
