import ctypes


SCOM_FRAME_HEADER_SIZE = 14

# scom_error_t
SCOM_ERROR_NO_ERROR = 0 # /home/gavin/tmp/scomlib/scom_data_link.h: 117
SCOM_ERROR_INVALID_FRAME = 1 # /home/gavin/tmp/scomlib/scom_data_link.h: 117
SCOM_ERROR_DEVICE_NOT_FOUND = 2 # /home/gavin/tmp/scomlib/scom_data_link.h: 117
SCOM_ERROR_RESPONSE_TIMEOUT = 3 # /home/gavin/tmp/scomlib/scom_data_link.h: 117
SCOM_ERROR_SERVICE_NOT_SUPPORTED = 17 # /home/gavin/tmp/scomlib/scom_data_link.h: 117
SCOM_ERROR_INVALID_SERVICE_ARGUMENT = 18 # /home/gavin/tmp/scomlib/scom_data_link.h: 117
SCOM_ERROR_GATEWAY_BUSY = 19 # /home/gavin/tmp/scomlib/scom_data_link.h: 117
SCOM_ERROR_TYPE_NOT_SUPPORTED = 33 # /home/gavin/tmp/scomlib/scom_data_link.h: 117
SCOM_ERROR_OBJECT_ID_NOT_FOUND = 34 # /home/gavin/tmp/scomlib/scom_data_link.h: 117
SCOM_ERROR_PROPERTY_NOT_SUPPORTED = 35 # /home/gavin/tmp/scomlib/scom_data_link.h: 117
SCOM_ERROR_INVALID_DATA_LENGTH = 36 # /home/gavin/tmp/scomlib/scom_data_link.h: 117
SCOM_ERROR_PROPERTY_IS_READ_ONLY = 37 # /home/gavin/tmp/scomlib/scom_data_link.h: 117
SCOM_ERROR_INVALID_DATA = 38 # /home/gavin/tmp/scomlib/scom_data_link.h: 117
SCOM_ERROR_DATA_TOO_SMALL = 39 # /home/gavin/tmp/scomlib/scom_data_link.h: 117
SCOM_ERROR_DATA_TOO_BIG = 40 # /home/gavin/tmp/scomlib/scom_data_link.h: 117
SCOM_ERROR_WRITE_PROPERTY_FAILED = 41 # /home/gavin/tmp/scomlib/scom_data_link.h: 117
SCOM_ERROR_READ_PROPERTY_FAILED = 42 # /home/gavin/tmp/scomlib/scom_data_link.h: 117
SCOM_ERROR_ACCESS_DENIED = 43 # /home/gavin/tmp/scomlib/scom_data_link.h: 117
SCOM_ERROR_OBJECT_NOT_SUPPORTED = 44 # /home/gavin/tmp/scomlib/scom_data_link.h: 117
SCOM_ERROR_MULTICAST_READ_NOT_SUPPORTED = 45 # /home/gavin/tmp/scomlib/scom_data_link.h: 117
SCOM_ERROR_INVALID_SHELL_ARG = 129 # /home/gavin/tmp/scomlib/scom_data_link.h: 117
SCOM_ERROR_STACK_PORT_NOT_FOUND = 130 # /home/gavin/tmp/scomlib/scom_data_link.h: 117
SCOM_ERROR_STACK_PORT_INIT_FAILED = 131 # /home/gavin/tmp/scomlib/scom_data_link.h: 117
SCOM_ERROR_STACK_PORT_WRITE_FAILED = 132 # /home/gavin/tmp/scomlib/scom_data_link.h: 117
SCOM_ERROR_STACK_PORT_READ_FAILED = 133 # /home/gavin/tmp/scomlib/scom_data_link.h: 117
SCOM_ERROR_STACK_BUFFER_TOO_SMALL = 134 # /home/gavin/tmp/scomlib/scom_data_link.h: 117
SCOM_ERROR_STACK_PROPERTY_HEADER_DOESNT_MATCH = 135 # /home/gavin/tmp/scomlib/scom_data_link.h: 117

# scom_service_t
SCOM_READ_PROPERTY_SERVICE = 1 # /home/gavin/tmp/scomlib/scom_data_link.h: 126
SCOM_WRITE_PROPERTY_SERVICE = 2 # /home/gavin/tmp/scomlib/scom_data_link.h: 126

# scom_format_t
SCOM_FORMAT_INVALID_FORMAT = 0 # /home/gavin/tmp/scomlib/scom_data_link.h: 152
SCOM_FORMAT_BOOL = 1 # /home/gavin/tmp/scomlib/scom_data_link.h: 152
SCOM_FORMAT_FORMAT = 2 # /home/gavin/tmp/scomlib/scom_data_link.h: 152
SCOM_FORMAT_ENUM = 3 # /home/gavin/tmp/scomlib/scom_data_link.h: 152
SCOM_FORMAT_ERROR = 4 # /home/gavin/tmp/scomlib/scom_data_link.h: 152
SCOM_FORMAT_INT32 = 5 # /home/gavin/tmp/scomlib/scom_data_link.h: 152
SCOM_FORMAT_FLOAT = 6 # /home/gavin/tmp/scomlib/scom_data_link.h: 152
SCOM_FORMAT_STRING = 7 # /home/gavin/tmp/scomlib/scom_data_link.h: 152
SCOM_FORMAT_DYNAMIC = 8 # /home/gavin/tmp/scomlib/scom_data_link.h: 152
SCOM_FORMAT_BYTE_STREAM = 9 # /home/gavin/tmp/scomlib/scom_data_link.h: 152

# scom_object_type_t
SCOM_USER_INFO_OBJECT_TYPE = 1
SCOM_PARAMETER_OBJECT_TYPE = 2



class FrameFlags(ctypes.Structure):
    __slots__ = [
        'reserved7to5',
        'is_new_datalogger_file_present',
        'is_sd_card_full',
        'is_sd_card_present',
        'was_rcc_reseted',
        'is_message_pending',
    ]

    _fields_ = [
        ('reserved7to5', ctypes.c_int, 3),
        ('is_new_datalogger_file_present', ctypes.c_int, 1),
        ('is_sd_card_full', ctypes.c_int, 1),
        ('is_sd_card_present', ctypes.c_int, 1),
        ('was_rcc_reseted', ctypes.c_int, 1),
        ('is_message_pending', ctypes.c_int, 1),
    ]

class ServiceFlags(ctypes.Structure):
    __slots__ = [
        'reserved7to2',
        'is_response',
        'error',
    ]

    _fields_ = [
        ('reserved7to2', ctypes.c_int, 6),
        ('is_response', ctypes.c_int, 1),
        ('error', ctypes.c_int, 1),
    ]

c_enum = ctypes.c_int

class Frame(ctypes.Structure):
    _fields_ = [
        ('frame_flags', FrameFlags),
        ('src_addr', ctypes.c_uint32),
        ('dst_addr', ctypes.c_uint32),
        ('service_flags', ServiceFlags),
        ('service_id', c_enum),
        ('data_length', ctypes.c_size_t),
        ('last_error', c_enum),
        ('buffer', ctypes.POINTER(ctypes.c_char)),
        ('buffer_size', ctypes.c_size_t),
    ]

class Property(ctypes.Structure):
    _fields_ = [
        ('frame', ctypes.POINTER(Frame)),
        ('object_type', c_enum),
        ('object_id', ctypes.c_uint32),
        ('property_id', ctypes.c_uint16),
        ('value_length', ctypes.c_size_t),
        ('value_buffer', ctypes.POINTER(ctypes.c_ubyte)),
        ('value_buffer_size', ctypes.c_size_t),
    ]

