class XcomException(Exception):
    "All Xcom exceptions inherit this guy"
    def __init__(self, message, error_code=None):
        self.error_code = error_code

        super(XcomException, self).__init__(message)

class XcomReadException(XcomException):
    "Error reading a particular value"
    def __init__(self, message, error_code=None):
        if error_code is not None:
            message += " - {0} ({1})".format(
                    _error_code_to_message(error_code), error_code)

        super(XcomReadException, self).__init__(message, error_code)

class XcomWriteException(XcomException):
    "Error writing a particular value"
    def __init__(self, message, error_code=None):
        if error_code is not None:
            message += " - {0} ({1})".format(
                    _error_code_to_message(error_code), error_code)

        super(XcomWriteException, self).__init__(message, error_code)

class XcomSerialError(XcomException):
    "Something went wrong accessing the serial port"
    pass

def _error_code_to_message(code):
    errors = {
            0: "No error",
            1: "Invalid frame",
            2: "Device not found",
            3: "Response timeout",
            17: "Service not supported",
            18: "Invalid service argument",
            19: "Gateway is busy",
            33: "Type not supported",
            34: "Object ID not found",
            35: "Property not supported",
            36: "Invalid data length",
            37: "Property is read only",
            38: "Invalid data",
            39: "Data too small",
            40: "Data too big",
            41: "Write property failed",
            42: "Read property failed",
            43: "Access denied",
            44: "Object not supported",
            45: "Multicast read not supported",
            129: "Invalid shell argument",
            130: "Serial port not found",
            131: "Serial port init failed",
            132: "Serial port write failed",
            133: "Serial port read failed",
            134: "Buffer is too small",
            135: "Property header doesn't match",
    }

    return errors.get(code, "Unknown error")
