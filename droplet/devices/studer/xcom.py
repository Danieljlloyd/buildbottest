from ctypes import *
import serial
import struct
import os
from threading import Lock

import structs
from .exceptions import *

try:
    libscom_path = os.path.dirname(__file__) + "/scomlib/scomlib_arm.so"
    libscom = cdll.LoadLibrary(libscom_path)
except OSError:
    # Binaries aren't available for this platform
    libscom = None

class StuderXcom(object):
    "Interfaces with an Xcom-232i box through a serial port"

    def __init__(self):
        self.ser = None
        self.target_device_id = 101
        self.mutex = Lock()

        if libscom is None:
            raise XcomException("scomlib library isn't available for this platform.")

    def initialize_serial_port(self):
        try:
            self.ser = serial.Serial('/dev/ttyUSB0', 38400, timeout=2, 
                            bytesize=serial.EIGHTBITS,
                            parity=serial.PARITY_EVEN, 
                            stopbits=serial.STOPBITS_ONE,
                            xonxoff=False,
                            rtscts=False,
                            dsrdtr=False)
        except ValueError as e:
            raise XcomSerialException("Problem configuring the serial port - " + str(e))
        except serial.SerialException as e:
            raise XcomSerialException("Could not open the serial port - " + str(e))

    def close_serial_port(self):
        self.ser.close()

    def clear_serial_port(self):
        self.ser.flush()

    def write_serial_port(self, data, byte_count):
        """Given a buffer, write byte_count bytes to the serial port and
        return the number of bytes actually written.
        """
        
        # print "Sending frame {0}".format(data[0:byte_count].encode('hex'))

        buf = bytearray(data[0:byte_count])
        try:
            written = self.ser.write(buf)
            return written

        except serial.SerialTimeoutException as e:
            raise XcomWriteException("Serial port write timed out")


    def read_serial_port(self, frame, start_byte, byte_count):
        """Given a frame, read byte_count bytes from the serial port
        and store the result in the frame's buffer property starting
        at start_byte.

        The number of bytes actually read is returned.

        If no data is available, the operation will time out as configured
        when setting up the serial port and 0 will be returned.
        """

        buff = bytearray(byte_count)
        bytes_read = self.ser.readinto(buff)

        buff_as_string = str(buff)
        
        # print "Recieve frame {0}".format(buff_as_string[0:bytes_read].encode('hex'))

        # Copy the bytearray into the frame's buffer
        for i in range(0, bytes_read):
            frame.buffer[start_byte + i] = buff_as_string[i]

        return bytes_read

    def read_value(self, id, format, object_type=1, property_id=1, device_address=None):
        frame = structs.Frame()
        prop = structs.Property()
        buff = create_string_buffer(1024)

        libscom.scom_initialize_frame(byref(frame), buff, sizeof(buff))
        libscom.scom_initialize_property(byref(prop), byref(frame))

        frame.src_addr = 1
        frame.dst_addr = device_address or self.target_device_id

        prop.object_type = object_type # 1 for measurements, 2 for parameters, 3 for message
        prop.object_id = id
        prop.property_id = property_id

        libscom.scom_encode_read_property(byref(prop))
    
        if frame.last_error != structs.SCOM_ERROR_NO_ERROR:
            raise XcomReadException(
                "Couldn't encode frame for object {0}".format(id),
                frame.last_error)

        try:
            self.exchange_frame(frame)
        except XcomException as e:
            raise XcomReadException(
                str(e) + " for object {0}".format(id), 
                e.error_code)

        # reuse the structure to save space
        libscom.scom_initialize_property(byref(prop), byref(frame))

        libscom.scom_decode_read_property(byref(prop))
        if frame.last_error != structs.SCOM_ERROR_NO_ERROR:
            raise XcomReadException(
                "Couldn't decode response for object {0}".format(id),
                frame.last_error)

        # The value is encoded as a float but stored in a buffer of bytes.
        # No worries, we just C-style cast it to a float and voila! (aka wallah!)
        if format == 'float':
            if prop.value_length != 4:
                raise XcomReadException(
                    "Wrong response data size for object {0}. Expected {1} but got {2}" \
                    .format(id, 4, prop.value_length))

            value = cast(prop.value_buffer, POINTER(c_float)).contents.value
        elif format == 'enum':
            if prop.value_length != 2:
                raise XcomReadException(
                    "Wrong response data size for object {0}. Expected {1} but got {2}" \
                    .format(id, 2, prop.value_length))

            value = cast(prop.value_buffer, POINTER(c_short)).contents.value
        elif format == 'bool':
            if prop.value_length != 1:
                raise XcomReadException(
                    "Wrong response data size for object {0}. Expected {1} but got {2}" \
                    .format(id, 1, prop.value_length))

            value = cast(prop.value_buffer, POINTER(c_bool)).contents.value
        else:
            value = None

        return value

    def write_value(self, id, format, value, device_id=None, permanent=False):
        """Write a value to a configurable object id on the XTM unit.

        NOTE: Be careful with permanent=True. Xtender serial protocol
        tech spec Para 4.5.4 states "the number of write on a single parameter 
        property is only garanted for 1000 write operations"
        
        """

        # TODO handle permanent parameter by writing to 1550 first

        frame = structs.Frame()
        prop = structs.Property()
        buff = create_string_buffer(1024)

        libscom.scom_initialize_frame(byref(frame), buff, sizeof(buff))
        libscom.scom_initialize_property(byref(prop), byref(frame))

        frame.src_addr = 1
        frame.dst_addr = device_id or self.target_device_id 

        prop.object_type = structs.SCOM_PARAMETER_OBJECT_TYPE
        prop.object_id = id
        prop.property_id = 5

        # TODO use format to encode the value correctly
        prop.value_length = 4
        cast(prop.value_buffer, POINTER(c_float)).contents.value = value

        libscom.scom_encode_write_property(byref(prop))

        if frame.last_error != structs.SCOM_ERROR_NO_ERROR:
            raise XcomWriteException(
                    "Frame encoding failed for object {0}".format(id),
                    frame.last_error)

        try:
            result = self.exchange_frame(frame) 
        except XcomException as e:
            raise XcomWriteException(
                    str(e) + " for object {0}".format(id),
                    e.error_code)

        # reuse the structure to save space
        libscom.scom_initialize_property(byref(prop), byref(frame))

        libscom.scom_decode_write_property(byref(prop))
        if frame.last_error != structs.SCOM_ERROR_NO_ERROR:
            raise XcomWriteException(
                    "Response decoding failed for object {0}".format(id),
                    frame.last_error)

    def exchange_frame(self, frame):
        with self.mutex:
            byte_count = 0
            self.clear_serial_port()

            libscom.scom_encode_request_frame(byref(frame))
            if frame.last_error != structs.SCOM_ERROR_NO_ERROR:
                raise XcomException("Link frame encoding failed", frame.last_error)

            # Send the request
            frame_length = libscom.scom_frame_length(byref(frame))
            byte_count = self.write_serial_port(frame.buffer, frame_length)
            if byte_count != frame_length:
                raise XcomException("Only wrote {0} bytes instead of {1}".format(
                        byte_count, frame_length), 
                    structs.SCOM_ERROR_STACK_PORT_WRITE_FAILED)

            libscom.scom_initialize_frame(byref(frame), frame.buffer, frame.buffer_size)

            memset(frame.buffer, 0, frame.buffer_size)

            # Read the response
            byte_count = self.read_serial_port(frame, 0, structs.SCOM_FRAME_HEADER_SIZE)
            if byte_count != structs.SCOM_FRAME_HEADER_SIZE:
                raise XcomException("Header read only gave {0} bytes instead of {1}".format(
                        byte_count, structs.SCOM_FRAME_HEADER_SIZE),
                    structs.SCOM_ERROR_STACK_PORT_READ_FAILED)

            libscom.scom_decode_frame_header(byref(frame))
            if frame.last_error != structs.SCOM_ERROR_NO_ERROR:
                raise XcomException("Link header decoding failed", frame.last_error)

            frame_length = libscom.scom_frame_length(byref(frame)) - structs.SCOM_FRAME_HEADER_SIZE
            byte_count = self.read_serial_port(frame, structs.SCOM_FRAME_HEADER_SIZE, frame_length)
            if byte_count != frame_length:
                raise XcomException("Data read only gave {0} bytes instead of {1}".format(
                        byte_count, frame_length),
                    structs.SCOM_ERROR_STACK_PORT_READ_FAILED)

            libscom.scom_decode_frame_data(byref(frame))
            if frame.last_error != structs.SCOM_ERROR_NO_ERROR:
                raise XcomException("Link data decoding failed", frame.last_error)


