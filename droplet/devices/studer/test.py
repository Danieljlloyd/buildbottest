from ctypes import *
import serial
import struct
from collections import namedtuple

import structs

libscom = cdll.LoadLibrary("./scomlib/scomlib.so")
ser = None

def go():
    initialize_serial_port()

    flash(101)
    
    Measure = namedtuple('Measure', ['id', 'format', 'label', 'units'])

    measures = [
            Measure(3000, 'float', 'Bat Voltage', 'V'),
            Measure(3001, 'float', 'Bat Temp', 'C'),
            Measure(3005, 'float', 'Bat Charge Current', 'Adc'),
            Measure(3007, 'float', 'State of Charge', '%'),
            Measure(3010, 'enum', 'Battery Cycle Phase', ''),
            Measure(3011, 'float', 'Input Voltage', 'Vac'),
            Measure(3012, 'float', 'Input Current', 'Aac'),
            Measure(3013, 'float', 'Input Power', 'kVA'),
            Measure(3021, 'float', 'Output Voltage', 'Vac'),
            Measure(3022, 'float', 'Output Current', 'Aac'),
            Measure(3023, 'float', 'Output Power', 'kVA'),
            Measure(3028, 'enum', 'Operating State', ''),
            Measure(3049, 'enum', 'On/Off', ''),
            Measure(3076, 'float', 'Battery Discharge (yesterday)', 'kWh'),
            Measure(3078, 'float', 'Battery Discharge (today)', 'kWh'),
            Measure(3080, 'float', 'Energy (yesterday)', 'kWh'),
            Measure(3081, 'float', 'Energy (today)', 'kWh'),
            Measure(3082, 'float', 'Consumer Energy (yesterday', 'kWh'),
            Measure(3083, 'float', 'Consumer Energy (today)', 'kWh'),
            Measure(3084, 'float', 'Input Freq', 'Hz'),
            Measure(3085, 'float', 'Output Freq', 'Hz'),
            Measure(3087, 'float', 'Output Active Power', 'W'),
            Measure(3088, 'float', 'Input Active Power', 'W')
    ]

    for measure in measures:
        val = read_value(measure.id, measure.format)
        print "{0} = {1} {2}".format(measure.label, val, measure.units)

def read_value(id, format):
    frame = structs.Frame()
    prop = structs.Property()
    buff = create_string_buffer(1024)

    libscom.scom_initialize_frame(byref(frame), buff, sizeof(buff))
    libscom.scom_initialize_property(byref(prop), byref(frame))

    frame.src_addr = 1
    frame.dst_addr = 101

    prop.object_type = structs.SCOM_USER_INFO_OBJECT_TYPE
    prop.object_id = id
    prop.property_id = 1

    libscom.scom_encode_read_property(byref(prop))

    if frame.last_error != structs.SCOM_ERROR_NO_ERROR:
        print "read property frame encoding failed with error %d" % frame.last_error
        return

    if exchange_frame(frame) != structs.SCOM_ERROR_NO_ERROR:
        print "exchange frame failed"
        return

    # reuse the structure to save space
    libscom.scom_initialize_property(byref(prop), byref(frame))

    libscom.scom_decode_read_property(byref(prop))
    if frame.last_error != structs.SCOM_ERROR_NO_ERROR:
        print "read property decoding failed with error %d" % frame.last_error
        return


    # The value is encoded as a float but stored in a buffer of bytes.
    # No worries, we just C-style cast it to a float and voila! (aka wallah!)
    if format == 'float':
        if prop.value_length != 4:
            print "invalid property data reponse size: {0}".format(prop.value_length)
            return
        value = cast(prop.value_buffer, POINTER(c_float)).contents.value
    elif format == 'enum':
        if prop.value_length != 2:
            print "invalid property data reponse size: {0}".format(prop.value_length)
            return
        value = cast(prop.value_buffer, POINTER(c_short)).contents.value
    else:
        value = None

    return value

def flash(device_id):
    frame = structs.Frame()
    prop = structs.Property()
    buff = create_string_buffer(1024)

    libscom.scom_initialize_frame(byref(frame), buff, sizeof(buff))
    libscom.scom_initialize_property(byref(prop), byref(frame))

    frame.src_addr = 1
    frame.dst_addr = 502

    prop.object_type = structs.SCOM_PARAMETER_OBJECT_TYPE
    prop.object_id = 5119
    prop.property_id = 5
    prop.value_length = 4

    cast(prop.value_buffer, POINTER(c_float)).contents.value = device_id

    libscom.scom_encode_write_property(byref(prop))

    if frame.last_error != structs.SCOM_ERROR_NO_ERROR:
        print "read property frame encoding failed with error %d" % frame.last_error
        return

    if exchange_frame(frame) != structs.SCOM_ERROR_NO_ERROR:
        print "exchange frame failed"
        return

    # reuse the structure to save space
    libscom.scom_initialize_property(byref(prop), byref(frame))

    libscom.scom_decode_write_property(byref(prop))
    if frame.last_error != structs.SCOM_ERROR_NO_ERROR:
        print "write property decoding failed with error %d" % frame.last_error
        return


def exchange_frame(frame):
    byte_count = 0
    clear_serial_port()
    
    libscom.scom_encode_request_frame(byref(frame))
    if frame.last_error != structs.SCOM_ERROR_NO_ERROR:
        print "data link frame encoding failed with error %d" % frame.last_error
        return frame.last_error

    # Send the request
    frame_length = libscom.scom_frame_length(byref(frame))
    byte_count = write_serial_port(frame.buffer, frame_length)
    if byte_count != frame_length:
        print "error when writing to the com port"
        return structs.SCOM_ERROR_STACK_PORT_WRITE_FAILED

    libscom.scom_initialize_frame(byref(frame), frame.buffer, frame.buffer_size)

    memset(frame.buffer, 0, frame.buffer_size)

    # Read the response
    byte_count = read_serial_port(frame, 0, structs.SCOM_FRAME_HEADER_SIZE)
    if byte_count != structs.SCOM_FRAME_HEADER_SIZE:
        print "error when reading the header from the com port"
        return structs.SCOM_ERROR_STACK_PORT_READ_FAILED

    libscom.scom_decode_frame_header(byref(frame))
    if frame.last_error != structs.SCOM_ERROR_NO_ERROR:
        print "data link header decoding failed with error %d" % frame.last_error
        return frame.last_error

    frame_length = libscom.scom_frame_length(byref(frame)) - structs.SCOM_FRAME_HEADER_SIZE
    byte_count = read_serial_port(frame, structs.SCOM_FRAME_HEADER_SIZE, frame_length)
    if byte_count != frame_length:
        print "error when reading the data from the com port"
        return structs.SCOM_ERROR_STACK_PORT_READ_FAILED

    libscom.scom_decode_frame_data(byref(frame))
    if frame.last_error != structs.SCOM_ERROR_NO_ERROR:
        print "data link data decoding failed with error %d" % frame.last_error
        return frame.last_error

    return structs.SCOM_ERROR_NO_ERROR

def initialize_serial_port():
    global ser
    ser = serial.Serial('/dev/ttyUSB0', 38400, timeout=2, bytesize=serial.EIGHTBITS, 
            parity=serial.PARITY_EVEN, stopbits=serial.STOPBITS_ONE, 
            xonxoff=False,
            rtscts=False,
            dsrdtr=False)

def clear_serial_port():
    ser.flush()

def read_serial_port(frame, start_byte, byte_count):
    buff = bytearray(byte_count)
    bytes_read = ser.readinto(buff)
    
    buff_as_string = str(buff) 
    # print "Recieve frame {0}".format(buff_as_string[0:bytes_read].encode('hex'))

    # Copy the bytearray into the frame's buffer
    for i in range(0, bytes_read):
        frame.buffer[start_byte + i] = buff_as_string[i]

    return bytes_read

def write_serial_port(data, byte_count):
    # print "Sending frame {0}".format(data[0:byte_count].encode('hex'))
    
    buf = bytearray(data[0:byte_count])
    
    written = ser.write(buf)
    return written

def close_serial_port():
    ser.close()

go()
