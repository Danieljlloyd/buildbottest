import serial
import re
import io
from base import BaseDevice

########################################################################
#
# SwitchDin Voltornic RS232 Serial Driver
#
# Based upon "PIP-HS RS232 communication Protocol" document. This
# is not an official Voltronic source document.
#
# Deviations from the above are based upon analysis of messages via
# review of data exchanges using a terminal session to the device.
# Deviations are specifically identified in the source code below.
#
########################################################################
# Exceptions
########################################################################
class Voltronic_RS232_Exception( Exception ):
    # Base Exception class
    def __init__(self, message, error_code=None):
        self.error_code = error_code
        super(Voltronic_RS232_Exception, self).__init__(message)

class Voltronic_RS232_SerialException( Voltronic_RS232_Exception ):
    # Serial Port Exception
    pass

########################################################################
# Driver
########################################################################
class Voltronic_RS232(BaseDevice):
    def __init__( self, config):
        # Settings in Config File
        self.config = config

        #
        # Compile Regular Expressions For Messages
        #
        # QPIGS: (BBB.B CC.C DDD.D EE.E FFFF GGGG HHH III JJ.J KK OOO PPPP EEEE UUU.U WWW.W TTT.T b7b6b5b4b3b2b1b0 <CRC><cr>
        # -- Sample (242.5 000550 51.3 0000.3 242.8 00000 51.4 000.0 002 446.9 446.9 054.0 ---.- 099 00814 00550 ----- 176.5 ---.- ---.- 039.0 F---101010<84>'
        #              1      2    3     4      5     6    7    8     9   10    11    12    13    14  15    16    17    18     19   20     21    22       CRC
        #
        #
        #                                    1          2        3          4          5          6         
        self.QPIGSparser = re.compile("\(([0-9\.]{5}) ([0-9]{6}) ([0-9\.]{4}) ([0-9\.]{6}) ([0-9\.]{5}) " + \
                                      "([0-9]{5}) ([0-9\.]{4}) ([0-9\.]{5}) ([0-9]{3}) ([0-9\.]{5}) " + \
                                      "([0-9\.]{5}) ([0-9\.]{5}) ([0-9\.\-]{5}) ([0-9]{3}) ([0-9]{5}) " + \
                                      "([0-9]{5}) ([0-9\.\-]{5}) ([0-9\.\-]{5}) ([0-9\.\-]{5}) ([0-9\.\-]{5}) " + \
                                      "([0-9\.\-]{5}) ([A-Z])([\-0-9]{9})(.*)" )

        # Setup The Serial Interface Handles
        self.create_serial_interface()

        # Construct Parent Class
        super(Voltronic_RS232, self).__init__()

    @staticmethod
    def supports_device(identifier):
        return "Voltronic_RS232" in identifier

    #
    # Open The Serial Port and Wrap It In A TextIOWrapper
    #
    def create_serial_interface( self ):
        # Open The Serial Port
        try:
            self.ser = serial.Serial('/dev/ttyUSB0', 2400, timeout=2,
                            bytesize=serial.EIGHTBITS,
                            parity=serial.PARITY_NONE,
                            stopbits=serial.STOPBITS_ONE,
                            xonxoff=False,
                            rtscts=False,
                            dsrdtr=False)
        except ValueError as e:
            raise ("Problem configuring the serial port - " + str(e))
        except serial.SerialException as e:
            raise Voltronic_RS232_SerialException("Could not open the serial port - " + str(e))

        # Wrap Serial Port In Buffered IO
        # The Voltronic Protocol Uses Carriage Returns as end of message markers
        try:
            self.bufser = io.TextIOWrapper(io.BufferedRWPair(self.ser, self.ser, 1),
                                           errors='ignore',
                                           line_buffering = True,
                                           newline='\r')
        except:
            raise Voltronic_RS232_SerialException("Could not open the serial port as text I/O - " + str(e))
            self.ser = None
            self.bufser = None


    #
    # Close the serial port forecully
    #
    def teardown_serial_interface( self ):
        self.ser.close();
        self.ser = None
        self.bufser = None

    def heartbeat_tick(self):
        "Regular housekeeping - we don't have any"
        pass

    #
    # Ask the device something and bundle up the response
    #
    def query_device( self, command_string, response_parser ):
        # Build The Device Interface If It Does Not Exist
        if self.bufser is None:
            self.create_serial_interface()

        # Send command_string to the device
        try:
            self.bufser.write( unicode(command_string) )
        except:
            # Ditch This Sample. Try Again Next Cycle
            raise Voltronic_RS232_SerialException( "Voltronic  : Error Writing To Serial Device" )

        # Receive the response
        try:
            response = self.bufser.readline()
        except Exception as e:
            # Ditch This Sample. Try Again Next Cycle.
            raise Voltronic_RS232_SerialException( "Voltronic  : Error Reading From Serial Device" + str(e) )

        # Parse the response
        try:
            tokens = response_parser.match( response )
        except:
            # Error Parsing Serial Response
            # Probably a data or regex definition error ... soldier on for now
            raise Voltronic_RS232_SerialException( "Error Parsing Response From Voltronic Serial Device -- " + command_string + " -- " + response  )

        # Convert Response To Preferred Types : float then string
        extractedValues = []
        try:
            for aMatch in tokens.groups():
                # Prefer Floats
                try:
                    aValue = float(aMatch)
                    extractedValues.append( aValue )
                    continue
                except:
                    # Try Next
                    pass
    
                # Not A Number? Assume it's a string
                try:
                    aValue = str(aMatch)
                    extractedValues.append( aValue )
                except:
                    # Don't want out of order returns
                    extractedValues.append( "" )
        except:
            raise Voltronic_RS232_SerialException( "Error Decoding Voltronic Response -- " + command_string + " -- " + response  )
    
        print "Voltronic_RS232 : " + command_string + "\n-- " + response + "\n++ " + str(extractedValues) + "\n"

        return( extractedValues )

    def measurement_values(self):
        "Read all the values from the inverter"

        # Measurements
        measurements = []

        # MEASIN - Input Power Measurements - From PV Array
        values = self.query_device( 'QPIGS\r', self.QPIGSparser )
        print values
        # Grid Side Measuremenets
        measurements.append(('SWDINPV.VTRIC.MMXN3.Vol.instMag[MX]',   values[0]))
        measurements.append(('SWDINPV.VTRIC.MMXN3.Cur.instMag[MX]',   values[1]/1000.0))
        measurements.append(('SWDINPV.VTRIC.MMXN3.Freq.instMag[MX]',  values[2]))
        measurements.append(('SWDINPV.VTRIC.MMXN3.TotW.instMag[MX]',  values[3]))
        # Load Side Measurements
        measurements.append(('SWDINPV.VTRIC.MMXN2.Vol.instMag[MX]',   values[4]))
        measurements.append(('SWDINPV.VTRIC.MMXN2.Cur.instMag[MX]',   values[5]/1000.0))
        measurements.append(('SWDINPV.VTRIC.MMXN2.Freq.instMag[MX]',  values[6]))
        measurements.append(('SWDINPV.VTRIC.MMXN2.TotW.instMag[MX]',  values[7]))
        # PV Measurements
        measurements.append(('SWDINPV.VTRIC.MMXN1.Vol.instMag[MX]',   values[9]))
        # Battery Measurements
        measurements.append(('SWDINPV.VTRIC.ZBAT1.Vol.instMag[MX]',   values[11]))
        # System Measurements
        measurements.append(('SWDINPV.VTRIC.Tmp.instMag[MX]',         values[20]))


        return measurements

    def setting_values(self):
        "Read all setting values from the inverter"
        output = []

        # Device Identification And Serial Number
        #values = self.query_device( 'IDN?\r', self.IDNparser )
        #output.append( ('DeviceType',  values[0]) )
        #output.append( ('XanbusID',  values[1]) )
        #output.append( ('SerialNumber',  values[1]) )

        return output

    def command(self, command_name, value=None):
        "Receive commands."
        pass
