import serial
import modbus_tk
import modbus_tk.defines as cst
import modbus_tk.modbus_tcp as modbus_tcp
import modbus_tk.modbus_rtu as modbus_rtu

from collections import namedtuple
from struct import pack, unpack
import json
import glob

from base import BaseDevice

class ModbusDevice(BaseDevice):
    def __init__(self, config):
        super(ModbusDevice, self).__init__()
        self.config = config
        self.master = None
        self.ports = []
        self.ports_iter = iter(self.ports)
        self.init_modbus()

    def init_modbus(self):
        if self.config['modbus']['type'] == 'TCP':
            self.master = modbus_tcp.TcpMaster(host=self.config['modbus']['ip'], port=self.config['modbus']['port'])

        elif self.config['modbus']['type'] == 'RTU':
            try:
              comport = next(self.ports_iter)
            except StopIteration:
              self.ports = glob.glob(self.config['modbus']['device'])
              self.ports_iter = iter(self.ports)
              if len(self.ports):
                comport = next(self.ports_iter)
              else:
                print "Modbus RTU: No comms ports found"
                return

            print "Modbus RTU: Trying to connect to port {0}".format(comport)
            self.master = modbus_rtu.RtuMaster(serial.Serial(port=comport,
                                                             baudrate=self.config['modbus']['baudrate'],
                                                             bytesize=self.config['modbus']['bytesize'],
                                                             parity=self.config['modbus']['parity'],
                                                             stopbits=self.config['modbus']['stopbits'],
                                                             xonxoff=self.config['modbus']['xonxoff'],
                                                             timeout=5))

    @staticmethod
    def supports_device(identifier):
        return "Modbus" in identifier


    def heartbeat_tick(self):
        "Regular housekeeping - we don't have any"
        pass

    def measurement_values(self):
        "Read all the values from modbus"

        output = []

        if self.master == None:
            self.init_modbus()
            return output

        measures = self.config['modbus']['measurements']

        # Read each from the physical devices
        for measure in measures:
            try:
                # for now only support reading one value at a time.
                valuetuple = self.master.execute(measure['slaveid'], measure['functioncode'], measure['address'], measure['length'], data_format=measure['dataformat'].encode('ascii','ignore'))

                #debugging
                #print "value tuple {0}".format(valuetuple)
                #print measure['convertformat']
                #print measure['dataformat']

                if measure['reversebyteorder']:
                  decodetuple = valuetuple[::-1]
                else:
                  decodetuple = valuetuple

                offset = measure.get('offset', 0)

                # Scale and offset returned value
                value = unpack(measure['convertformat'].encode('ascii','ignore'), pack(measure['dataformat'].encode('ascii','ignore'), *decodetuple))[0] * measure['multiplier'] + offset

                # Check if value is in acceptable range, if so then add to output
                minvalue = measure.get('minvalue', -0xFFFFFFFF)
                maxvalue = measure.get('maxvalue', 0xFFFFFFFF) 
                if(value > minvalue and value < maxvalue):
                    output.append((measure['label'], value))

            except modbus_tk.modbus.ModbusError, e:
                # Just log and continue to the next measure
                print "Modbus error ", e.get_exception_code()
                self.master = None
                self.init_modbus()

            except Exception, e2:
                print "Error ", str(e2)
                self.master = None
                self.init_modbus()

        return output

    def setting_values(self):
        output = []
        return output

    def command(self, command_name, value=None):
        "Receive commands."
        print "Modbus COMMAND received: {0}:{1}".format(command_name, value)
        if command_name == "SETSWOOP":
            # Sets a value for a SWOOP label
            self.setswoop(value)
            return True

    def setswoop(self, value):

        if self.master == None:
          return

	try:
          valdict = json.loads(value)
	except:
          valdict = value

        metric = valdict['metric']

        commands = self.config['modbus']['commands']

        #print "SETSWOOP:"

        for command in commands:
          if command['command'] == 'SETSWOOP' and command['label'] == metric:

            #print "Found metric"
            val = valdict['value'] * command['multiplier']

            encodetuple = unpack(command['dataformat'].encode('ascii','ignore'), pack(command['convertformat'].encode('ascii','ignore'), val))

            if command['reversebyteorder']:
              valuetuple = encodetuple[::-1]
            else:
              valuetuple = encodetuple

            print valuetuple
            self.master.execute(command['slaveid'], command['functioncode'], command['address'], output_value=valuetuple)
