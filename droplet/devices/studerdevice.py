from collections import namedtuple

from base import BaseDevice
from studer.xcom import StuderXcom
from studer.exceptions import XcomException

class StuderBatteryDevice(BaseDevice):
    def __init__(self, config):
        super(StuderBatteryDevice, self).__init__()

        self.config = config
        self.xcom = None
        try:
            self.xcom = StuderXcom()
            self.xcom.initialize_serial_port()
        except XcomException as e:
            print e

    @staticmethod
    def supports_device(identifier):
        return "Studer" in identifier

 
    def heartbeat_tick(self):
        "Regular housekeeping - we don't have any"
        pass

    def measurement_values(self):
        "Read all the values from the inverter"

        if not self.xcom:
            raise XcomException("Couldn't read Studer values - xcom not available")

        Measure = namedtuple('Measure', ['address', 'id', 'format', 'label', 'units', 'multiplier'])

        measures = []

        for measure_config in self.config.get('measures', []):
            measure = Measure(
                measure_config.get("deviceaddress", 101),
                measure_config.get("measureid", 0),
                measure_config.get("type", "float"),
                measure_config.get("name", ""),
                measure_config.get("units", ""),
                measure_config.get("multiplier", 1)
            )

            measures.append(measure)

            # Measures are now defined in the config file
            #Measure(3000, 'float', 'SWDINPV.ZBAT1.Vol.instMag[MX]', 'V'), # Battery Voltage
            #Measure(3001, 'float', 'SWDINPV.STMP1.Tmp.instMag[MX]', 'C'), # Battery Temperature
            #Measure(3005, 'float', 'bat_charge_current', 'Adc'),
            #Measure(3007, 'float', 'bat_charge_percent', '%'),
            #Measure(3010, 'enum', 'bat_cycle_phase', ''),
            #Measure(3011, 'float', 'input_voltage', 'Vac'),
            #Measure(3012, 'float', 'input_current', 'Aac'),
            #Measure(3013, 'float', 'SWDINPV.MMXN1.TotW.instMag[MX]', 'kVA'), # Input Power - Generated
            #Measure(3021, 'float', 'output_voltage', 'Vac'),
            #Measure(3022, 'float', 'output_current', 'Aac'),
            #Measure(3023, 'float', 'SWDINPV.MMXN2.TotW.instMag[MX]', 'kVA'), # Output Power - Load
            #Measure(3028, 'enum', 'operating_state', ''),
            #Measure(3049, 'enum', 'on_state', ''),
            #Measure(3076, 'float', 'bat_discharge_yesterday', 'kWh'),
            #Measure(3078, 'float', 'bat_discharge_today', 'kWh'),
            #Measure(3080, 'float', 'input_energy_yesterday', 'kWh'),
            #Measure(3081, 'float', 'input_energy_today', 'kWh'),
            #Measure(3082, 'float', 'output_energy_yesterday', 'kWh'),
            #Measure(3083, 'float', 'output_energy_today', 'kWh'),
            #Measure(3084, 'float', 'input_frequency', 'Hz'),
            #Measure(3085, 'float', 'output_frequency', 'Hz'),
            #Measure(3087, 'float', 'output_active_power', 'W'),
            #Measure(3088, 'float', 'input_active_power', 'W')

        # Read each from the physical device
        output = []
        for measure in measures:
            try:
                value = self.xcom.read_value(measure.id, measure.format, 
                        device_address=measure.address)

                if measure.format == "float" and measure.multiplier != 1:
                    value *= measure.multiplier

                output.append((measure.label, value))
            except XcomException as e:
                # Wrap the exception with a nicer message
                raise XcomException("Communication with the Studer failed. Either the device is off or the Xcom box is unplugged ({0})".format(e.message))

        return output

    def setting_values(self):
        "Read all setting values from the inverter"

        if not self.xcom:
            raise XcomException("Couldn't read Studer settings - xcom not available")

        Measure = namedtuple('Measure', ['id', 'format', 'label', 'units'])

        # Require SWOOP Metric names for these.
        measures = [
            #Measure(1107, 'float', 'input_current_max', ''),
            #Measure(1138, 'float', 'bat_charge_current_max', ''),
            #Measure(1126, 'bool', 'smart_boost_enable', ''),
            #Measure(1124, 'bool', 'inverter_enable', ''),
            #Measure(1125, 'bool', 'charger_allowed', ''),
            #Measure(1108, 'float', 'bat_voltage_noload_min', ''),
            #Measure(1109, 'float', 'bat_voltage_load_min', ''),
            #Measure(1121, 'float', 'bat_voltage_max', ''),
            #Measure(1127, 'bool', 'grid_feeding_enable', ''),
            #Measure(1523, 'float', 'grid_feeding_current_max', ''),
        ]

        # Read each from the physical device
        output = []
        for measure in measures:
            try:
                value = self.xcom.read_value(
                        measure.id, measure.format, object_type=2, property_id=5)
                output.append((measure.label, value))
            except XcomException as e:
                # Wrap the exception with a nicer message
                raise XcomException("Communication with the Studer failed. Either the device is off or the Xcom box is unplugged ({0})".format(e.message))

        return output

    def command(self, command_name, value=None):
        "Receive commands."
        if command_name == "FLASH":
            # Flash the lights on the front panel
            self.flash()
            return True 

    def flash(self):
        self.xcom.write_value(5119, 'float', 101, device_id=501)

      
