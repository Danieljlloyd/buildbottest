import random
from base import BaseDevice

from math import pi, radians, degrees, sin, tan, cos, acos, floor
from datetime import datetime, timedelta, time


class VerySimpleDevice(BaseDevice):
  def __init__(self, config):
    # Measurements
    self.temperature = 28.0
    self.battery = [80.0, 80.0] # previous value, current value
    self.power = 2000.0
    
    # Actuators
    self.charging = False
    
    # Battery charging model constants
    self.alpha = 0.5 	#time constant (seconds)
    self.gain = 1 	#throughput gain (%/%)
   

  @staticmethod
  def supports_device(identifier):
    return identifier in ["Very Simple Simulator"]

 
  def heartbeat_tick(self):
    """ Updates all the measurements to the next increment of time in the simulation. """
    
    if self.temperature > 25.0:
        self.temperature += random.uniform(-0.7, 0.5)
    else:
        self.temperature += random.uniform(-0.5, 0.7)

    # Randomly turn on/off charger
    if random.uniform(0.0, 10.0) < 1.0:
        self.charging = not self.charging

    
    y1 = self.battery[0]
    y2 = self.battery[1]
    
    xt = 100 if self.charging else 0     #turn on/off charger unit
    
    dt = 0.1
    yt = (self.gain * dt * xt + y2) / (1 + self.alpha * dt)
    
    if yt < 0: yt = 0
    if yt > 100: yt = 100    
    	
    self.battery[0] = y2
    self.battery[1] = yt
    
    # Newcastle values
    lat = -32.92
    lng = 151.8
    k_t = 0.60
    d = datetime.now()
    
    # Report solar irradiation as 'power'
    diffuse, radiation, sunset = solar_rad(lat, lng, k_t, d)
    self.power = diffuse + radiation

  def measurement_values(self):
    return [('SWDINplant.STMP1.Tmp.instMag[MX]', self.temperature), # Outside Temp
             ('SWDINPV.ZBAT1.Vol.instMag[MX]', self.battery[1]), # Battery Voltage
             ('SWDINPV.MMXN1.TotW.instMag[MX]', self.power), # Generated Power
             ('SWDINPV.MMXN3.TotW.instMag[MX]', self.power)] # Grid Feedin Power
    
  def setting_values(self):
    return []

  def command(self, command_name, value=None):
    if command_name == "BATTERY ON":
      self.charging = True
    elif command_name == "BATTERY OFF":
      self.charging = False
      


def solar_rad(lat, lng, k_t, t):
    """
    (ported from Matlab on 18-Feb-14)    
    
    SOLAR_RAD [g_d_h, g_b_h,w_s] = SOLAR_RAD(lat,long,k_t,t)
    
    OUT: Hourly diffuse (g_d_h, W/m^2) and beam radiation (g_b_h, W/m^2) 
    components for solar radiaton in the horizontal plan and the sunset time
    (t_sunset, [HH MM SS]) for that day
    IN: latitute (lat, decimal degrees) and longitude (long, decimal degrees),
    with clearness index (k_t) and 
    at the present time ([year,month,day,hour,second], decimal)
    
    Refs: equations from "Photovoltaic Project Analysis", RETScreen International
    or from Duffie & Beckman (2006)
    
    by Andrew Mears 2 Feb 2014
    rev 18 Feb 2014
    """
    
    #constants
    G_SC = 1367; # solar constant W/m2
    
    # convert lat and long from Degrees to Radians
    lat = radians(lat);
    lng = radians(lng);
    
    # current time angle (w_t, Radians) being 15Deg for every hour away from
    # solar noon (taken as local time - so some error here)
    
    w_t = radians(15) * (t.hour - 12)
    
    # determine the day number for today relative to 1 Jan (1 indexed)
    n = t.timetuple().tm_yday
    
    # Calculate average declination angle (radians) by Cooper's equation
    # decl = 23.45*sin(2*pi*(284+n)/365)*pi/180
    
    # ... same by Spencer's equation (see D&B eq 1.6.1b)
    B = 2 * pi * (n - 1) / 365.0;
    decl = 0.006918 - 0.399912 * cos(B) + 0.070257 * sin(B) \
        - 0.006758 * cos(2 * B) + 0.000907 * sin(2 * B) \
        - 0.002697 * cos(3 * B) + 0.00148 * sin(3 * B)
    
    # Calculate sunset and sunrise hour angle (radians) and time of sunset
    # for this day
    w_s = acos(-tan(lat) * tan(decl))
    w_r = -w_s
    w_s_time = degrees(w_s) / 15
    sunset_hours = int(w_s_time) # Integer part of w_s_time
    sunset_minutes = int(round((w_s_time - floor(w_s_time)) * 60)) # Decimal part of w_s_time
    
    if sunset_minutes >= 60:
        sunset_minutes = 0
        sunset_hours += 1    
    
    t_sunset = time(sunset_hours, sunset_minutes, 0)
    
    # Calculate average extraterrestrial radiation incident on the normal plane
    # (J/m2)
    h_0 = (86400 * G_SC / pi) * (1 + 0.033 * cos(2 * pi * n / 365)) * (cos(lat) * cos(decl) * sin(w_s) \
        + w_s * sin(lat) * sin(decl))
    
    # Calculate average (ground level) radiation incident on the normal plane
    # (J/m2)
    h = k_t * h_0
    
    # Calculate average daily diffuse global irradiance (J/m2)
    if w_s < radians(81.4):
        h_d = (1.391 - 3.560 * k_t + 4.189 * k_t**2 - 2.137 * k_t**3) * h;
    else:
        h_d = (1.311 - 3.022 * k_t + 3.427 * k_t**2 - 1.821 * k_t**3) * h;
    
    
    # Calculate average daily radiation
    a = 0.409 + 0.5016 * sin(w_s - pi / 3);
    b = 0.6609 - 0.4767 * sin(w_s - pi / 3);
    
    # calculate ratio of hourly total to daily total diffuse radiation
    r_d = (pi / 24) * (cos(w_t) - cos(w_s)) / (sin(w_s) - w_s * cos(w_s))
    # calculate ratio of hourly total to daily total global radiation
    r_t = r_d * (a + b * cos(w_t))
    
    
    # return global horizontal irradiance for the hour
    h_h = r_t * h;
    # return diffuse irradiance for the hour
    h_d_h = r_d * h_d;
    g_d_h = h_d_h / 3600; # convert to average W/m^2 
    # return beam irradiance component for the hour
    h_b_h = h_h - h_d_h;
    g_b_h = h_b_h / 3600; # convert to average W/m^2
    
    # Prevent negative irradiance
    if g_d_h < 0: g_d_h = 0
    if g_b_h < 0: g_b_h = 0
    
    return (g_d_h, g_b_h, t_sunset)


# Test script
if __name__ == "__main__":
    """# Wisconsin test values
    lat = 43.09;
    lng = 89.43;
    k_t = 0.55;
    d = datetime.now() #datetime(2014, 6,11,13,30)"""
    
    # Newcastle values
    lat = -32.92
    lng = 151.8
    k_t = 0.70
    d = datetime.now()
    print solar_rad(lat, lng, k_t, d)
      
