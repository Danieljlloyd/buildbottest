#!/usr/bin/env python

import re
import threading
import time 
# SwitchDin Driver Model
from base import BaseDevice
# CANbus Interface
import can
import socket
# Message Packing / Unpacking
from struct import *
from collections import namedtuple


"""
CAN storage class holds two dictionary buffers which take turns as read
and write buffers.The dictionary structure allows for messages from same 
arbitration ID's to be updated and then only the most current data is read 
from the reporter.
"""
class CanStorage:

	def __init__(self):
		self.buf1 = {}
		self.buf2 = {}
		self.readbuf = self.buf1
		self.writebuf = self.buf2

	"""
	Swap the reading and writing buffers, in preparation to read the 
	most recent data.  
	""" 
	def _swap(self):
		tmpbuf = self.writebuf
		self.writebuf = self.readbuf
		self.readbuf = tmpbuf

	"""
	Write the current message to the write buffer.
	"""
	def write(self, message):
		self.writebuf[message.arbitration_id] = message

	"""
	Return the current message buffer.
	"""
	def read(self):
		self._swap()
		self.writebuf = {}
		return self.readbuf.copy()


CanStore = CanStorage()

class CANUpdateThread(threading.Thread):
  def __init__(self, msgbuf):
    threading.Thread.__init__(self)
    self.MessageBuffer = msgbuf

  def run(self):
    while True:
      msg = self.MessageBuffer.get_message(timeout = 0.01)

      if msg is not None:
        CanStore.write(msg)
      else:
        time.sleep(0.2)

class CANbusDevice(BaseDevice):

  def __init__(self, config):
    super(CANbusDevice, self).__init__()
    self.config = config

    # Unpack the original dictionary into a new structure linking a regex to
    # a signal definition 
    for key, value in self.config['canbus']['measurements'].iteritems():
      self.config['canbus']['measurements'][key]['regex'] = re.compile(key)

    self.setupCANbus()

  def setupCANbus(self):
    # Configure the socketcan driver
    # slcan_attach
    # slcand
    # ifconfig

    # Open The Configured Device
    self.CANbus = can.interface.Bus(self.config['channel'], bustype=self.config['candriver']) 
    # Setup An Inbound Message Queue
    self.MessageBuffer = can.BufferedReader( )
    self.MessageNotifier = can.Notifier( self.CANbus, [self.MessageBuffer] )

    self.updateThread = CANUpdateThread(self.MessageBuffer)
    self.updateThread.daemon = True
    self.updateThread.start()

  def parse_message(self, message):
    # Lookup Message ID In Message Definitions
    if message.id_type:
      # This is an extanded address frame - Represented as 8 Hexidecimal Chars
      arbIdAsHex =  format(message.arbitration_id, '08X')
    else:
      # This is an normal address frame - Represented as 3 Hexidecimal Chars
      arbIdAsHex =  format(message.arbitration_id, '03X')

    # Check if Message Is In Config File
    msgDefn = None
    # Put defn into msgDefn on regex match in here.
    for key, value in self.config['canbus']['measurements'].iteritems():
      if(value['regex'].match(arbIdAsHex)):
        msgDefn = value

    if(not msgDefn):
      #print "Message " + arbIdAsHex + " received on CANbus has no decode defintion in config file"
      return []
      #raise

    # Build Message Decoder - Could be done during initialisation to speed up
    tupleNames = ""
    for aSignal in msgDefn['signals']:
      tupleNames = tupleNames + aSignal + " "

    # Decode Message
    unPacker = namedtuple( msgDefn['name'], tupleNames )
    signalDict = unPacker._asdict(unPacker._make( unpack( msgDefn['packformat'], message.data )))

    # Decode All Signals And Put Results In decodedSignals
    decodedSignals = []
    for aSignal, signalDef in msgDefn['signals'].items():
      # Scale the value based upon the config file settings
      signal_value = signalDict[aSignal] * signalDef['factor'] + signalDef['offset']

      # Signal Key Value
      signal_key = signalDef['key']

      # Append Results To Returned Data Set
      if signalDef['report']: 
        decodedSignals.append( (signal_key, signal_value) )

      #print "---", msgDefn['name'], ' - ', aSignal, " - ", signal_key, " - ", signalDict[aSignal], " - ", signal_value

    return decodedSignals

  # Identifier for Config File
  @staticmethod
  def supports_device(identifier):
    return "CANbus" in identifier
  
  # Periodic Processing
  def heartbeat_tick(self):
    pass
    
  # Return Measurements
  def measurement_values(self):
    # Report on current state 
    decodedSignals = []
    rawMsgDict = CanStore.read()
    for (arbid, msg) in rawMsgDict.iteritems():
      parsedMsg = self.parse_message(msg)
      decodedSignals = decodedSignals + parsedMsg

    return decodedSignals

  # Return Settings
  def setting_values(self):
    pass

  # Return Execute Command
  def command(self, command_name, value=None):
    pass
