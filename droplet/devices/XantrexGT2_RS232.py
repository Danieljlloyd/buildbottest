import serial
import re
import io
from base import BaseDevice

########################################################################
#
# SwitchDin Xantrex GT2 RS232 Serial Driver
#
# Based upon "SolarStorm RS232 Commands / Doc 503-0046-01-01 / Rev R"
# Deviations from the above are based upon analysis of messages via
# review of data exchanges using a terminal session to the device.
# Deviations are specifically identified in the source code below.
#
########################################################################
# Exceptions
########################################################################
class XantrexGT2_RS232_Exception( Exception ):
    # Base Exception class
    def __init__(self, message, error_code=None):
        self.error_code = error_code
        super(XantrexGT2_RS232_Exception, self).__init__(message)

class XantrexGT2_RS232_SerialException( XantrexGT2_RS232_Exception ):
    # Serial Port Exception
    pass

########################################################################
# Driver
########################################################################
class XantrexGT2_RS232(BaseDevice):
    def __init__( self, config):
        # Settings in Config File
        self.config = config

        #
        # Compile Regular Expressions For Messages
        #
        # MEASIN? : V:0.0 I:0.00 P:0 VB:0.0
        self.MEASINparser = re.compile("V:([0-9\.]+) I:([0-9\.]+) P:([0-9\.]+) VB:([0-9\.]+)")
        # MEASOUT? : V:0.0 I:0.00 P:0 F:0.0
        self.MEASOUTparser = re.compile("V:([0-9\.]+) I:([0-9\.]+) P:([0-9\.]+) F:([0-9\.]+)")
        # MEASTEMP? : C:0.0 F:32.0
        self.MEASTEMPparser = re.compile("C:([0-9\.]+) F:([0-9\.]+)")
        # IDN?? : M:GT2.8-AU-230 X:470057 S:B12653392
        self.IDNparser = re.compile("M:(\S+) X:(\S+) S:(\S+)")

        # Setup The Serial Interface Handles
        self.create_serial_interface()

        # Construct Parent Class
        super(XantrexGT2_RS232, self).__init__()

    @staticmethod
    def supports_device(identifier):
        return "XantrexGT2_RS232" in identifier

    #
    # Open The Serial Port and Wrap It In A TextIOWrapper
    #
    def create_serial_interface( self ):
        # Open The Serial Port
        try:
            self.ser = serial.Serial('/dev/ttyUSB0', 9600, timeout=2,
                            bytesize=serial.EIGHTBITS,
                            parity=serial.PARITY_NONE,
                            stopbits=serial.STOPBITS_ONE,
                            xonxoff=False,
                            rtscts=False,
                            dsrdtr=False)
        except ValueError as e:
            raise ("Problem configuring the serial port - " + str(e))
        except serial.SerialException as e:
            raise XantrexGT2_RS232_SerialException("Could not open the serial port - " + str(e))

        # Wrap Serial Port In Buffered IO
        # The Xantrex Protocol Uses Carriage Returns as end of message markers
        try:
            self.bufser = io.TextIOWrapper(io.BufferedRWPair(self.ser, self.ser, 1),
                                           errors='ignore',
                                           line_buffering = True,
                                           newline='\r')
        except:
            raise XantrexGT2_RS232_SerialException("Could not open the serial port as text I/O - " + str(e))
            self.ser = None
            self.bufser = None


    #
    # Close the serial port forecully
    #
    def teardown_serial_interface( self ):
        self.ser.close();
        self.ser = None
        self.bufser = None

    def heartbeat_tick(self):
        "Regular housekeeping - we don't have any"
        pass

    #
    # Ask the device something and bundle up the response
    #
    def query_device( self, command_string, response_parser ):
        # Build The Device Interface If It Does Not Exist
        if self.bufser is None:
            self.create_serial_interface()

        # Send command_string to the device
        try:
            self.bufser.write( unicode(command_string) )
        except:
            # Ditch This Sample. Try Again Next Cycle
            raise XantrexGT2_RS232_SerialException( "Xantrex GT2 : Error Writing To Serial Device" )

        # Receive the response
        try:
            response = self.bufser.readline()
        except Execption as e:
            # Ditch This Sample. Try Again Next Cycle.
            raise XantrexGT2_RS232_SerialException( "Xantrex GT2 : Error Reading From Serial Device" + str(e) )

        # Parse the response
        try:
            tokens = response_parser.match( response )
        except:
            # Error Parsing Serial Response
            # Probably a data or regex definition error ... soldier on for now
            raise XantrexGT2_RS232_SerialException( "Error Parsing Response From XantrexGT2 Serial Device -- " + command_string + " -- " + response  )

        # Convert Response To Preferred Types : float then string
        extractedValues = []
	try:
            for aMatch in tokens.groups():
                # Prefer Floats
                try:
                    aValue = float(aMatch)
                    extractedValues.append( aValue )
                    continue
                except:
                    # Try Next
                    pass
    
                # Not A Number? Assume it's a string
                try:
                    aValue = str(aMatch)
                    extractedValues.append( aValue )
                except:
                    # Don't want out of order returns
                    extractedValues.append( "" )
	except:
            raise XantrexGT2_RS232_SerialException( "Error Decoding XantrexGT2 Response -- " + command_string + " -- " + response  )
    
        print "XantrexGT2_RS232 : " + command_string + "\n-- " + response + "\n++ " + str(extractedValues) + "\n"

        return( extractedValues )

    def measurement_values(self):
        "Read all the values from the inverter"

        # Measurements
        measurements = []

        # MEASIN - Input Power Measurements - From PV Array
        values = self.query_device( 'MEASIN?\r', self.MEASINparser )
        measurements.append( ('SWDINPV.MPPT.MMXN1.Vol.instMag[MX]',  values[0]) )
        measurements.append( ('SWDINPV.MPPT.MMXN1.Cur.instMag[MX]',  values[1]) )
        measurements.append( ('SWDINPV.MPPT.MMXN1.TotW.instMag[MX]', values[2] / 1000.0) )

        # MEASOUT - Inverter Output Measurements - Load / AC Side
        values = self.query_device( 'MEASOUT?\r', self.MEASOUTparser )
        measurements.append(('SWDINPV.MPPT.MMXN2.Vol.instMag[MX]',  values[0]))
        measurements.append(('SWDINPV.MPPT.MMXN2.Cur.instMag[MX]',  values[1]))
        measurements.append(('SWDINPV.MPPT.MMXN2.TotW.instMag[MX]', values[2] / 1000.0))
        measurements.append(('SWDINPV.MPPT.MMXN2.Freq.instMag[MX]', values[3]))

        # Inverter Temperature
        values = self.query_device( 'MEASTEMP?\r', self.MEASTEMPparser )
        measurements.append(('SWDINPV.MPPT.MMXN2.Tmp.instMag[MX]',  values[0]))

        return measurements

    def setting_values(self):
        "Read all setting values from the inverter"
	output = []

	# Device Identification And Serial Number
        #values = self.query_device( 'IDN?\r', self.IDNparser )
        #output.append( ('DeviceType',  values[0]) )
        #output.append( ('XanbusID',  values[1]) )
        #output.append( ('SerialNumber',  values[1]) )

        return output

    def command(self, command_name, value=None):
        "Receive commands."
        pass
