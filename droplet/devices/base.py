from types import TypeType
import random

class BaseDevice(object):
  """
  Interface style class to describle what functions a device should supply.
  """

  @staticmethod
  def supports_device(identifier):
    """
    Checks identifier, to confirm we are a suitable class for supporting the device type or model.
    Called by the factory to create the right classes for the right devices.
    For example do:
        return identifier in ["Xantrex GT4000", "Xantrex GT5000"]
    """
    return False
  
  def heartbeat_tick(self):
    """
    Trigger the simulation to run or measurements to be made. The frequency of calls is defined by blast.
    """
    raise NotImplementedError()
    
  def measurement_values(self):
    """
    List of all measurements and values that should be reported to the server.
    
    These are values which change minute by minute without any user interaction.

    Return value is of the form: [('name', value), ('name', value)]
    """
    raise NotImplementedError()
    
  def setting_values(self):
    """
    List of all setting values that should be reported to the server.

    These are values than only change in response to operator intervention.
    
    Return value is of the form: [('name', value), ('name', value)]
    """
    raise NotImplementedError()

  def command(self, command_name, value=None):
    """
    If the server issues a command it will be called here, optionally with a value.
    """
    raise NotImplementedError()


class DeviceFactory(object):
    @staticmethod
    def newDevice(identifier, config={}):
        # Walk through all Device classes 
        deviceClasses = BaseDevice.__subclasses__()
        for deviceClass in deviceClasses:
            if deviceClass.supports_device(identifier):
                return deviceClass(config)
        # if research was unsuccessful, raise an error
        raise ValueError('No device module supporting "%s".' % identifier)
