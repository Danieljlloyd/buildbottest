from controllerBase import BaseController
from controllerBase import enum
from stateStore import stateStore

#
# WARNING : This controller is in a very developmental state. DO NOT use
#           until it's been finished.
#

#
# Simple Controller for setup at Newcastle Council Number 2 Sportsground.
#
# System consits of:
# 1. SMA Tripower Grid Connected PV Inverter
# 2. Voltronic Quattro Inverter/Charger
# 3. Battrium BMS + LiI Battery System
# 4. NOTE: There is NO grid power meter available
#
# The controller uses the following rules:
# 1. Battery Is Charged at off-peak tarrif / peak solar times : 11am to 2pm
chargeTimeStart = 11
chargeTimeEnd   = 14
# 2. Battery is discharged to support peaky load for lights (6pm to 9pm)
dischargeTimeStart = 18
dischargeTimeEnd = 21
# 3. During discharge, input current on the Voltronic is limited to 9A. Is
#    50A otherwise.
dischargeCurrentLimit = 10.5
gridFeedCurrentLimit = 50.0
# 4. Battery Voltage is always maintained above batLowerVoltLimit volts where possible
batLowerVoltLimit = 47.6
#
# Control is enabled through the Victron inverter charger. The CCGX device
# is used to communicate with the inverter via ModbusTCP.
#
# Controller Inputs:
#
# ModbusTCP To Controller Controler To Victron Quattro:
# 1. "Switch Position" (register 22): 1=Charger Only;2=Inverter Only;3=On;4=Off
inverterModes = enum(CHARGER=1, INVERTER=2, INVCHRGR=3, OFF=4)
# 2. "Active input current limit" (register 33)
# Note that the lowest controllable value seems to be 10.5A.
# If you request a setting lower than that, then the inverter sets it to 10.5.

class timeBasedBatteryManagement( BaseController ):

   # Class Constructor
   def __init__( self, config ):
     super(timeBasedBatteryManagement, self).__init__( )
     self.config = config
     self.currentState = "Grid Feed"

   # Identifier for Config File
   @staticmethod
   def implements(identifier):
      return "timeBasedBatteryManagement" in identifier

   #
   # Monitor System for state changes.
   # Manage State Transistions.
   # Communicate Change Actions To Droplet Main Module
   #
   def periodicProcessing( self ):

     #print "--- START --- NCC Periodic Processing"

     # Current System State Measurements
     self.currentBatteryVoltage    = self.stateStore.getVal('SWDINPV.VIC.ZBAT1.Vol.instMag[MX]')
     self.currentInverterMode      = self.stateStore.getVal('SWDINPV.VIC.Mode.instMag[MX]')
     self.currentInputCurrentLimit = self.stateStore.getVal('SWDINPV.VIC.InpCurLim.instMag[MX]')
     #  Desired System State
     self.desiredInverterMode      = self.currentInverterMode
     self.desiredInputCurrentLimit = self.currentInputCurrentLimit
     # Current Hour Of Day
     self.currentHour = self.stateStore.getVal('time')

     # Perform State Processing
     if(self.currentState == "Grid Feed"):
       self.stateGridFeed()
     elif(self.currentState == "Charge"):
       self.stateCharging()
     elif(self.currentState == "Discharge"):
       self.stateDisCharging()

     # Send Commands For State Change
     # Request inverter mode change
     actionList = []
     if self.desiredInverterMode != self.currentInverterMode :
       #print "Setting inverter to {0} from {1}".format( self.desiredInverterMode, self.currentInverterMode )
       actionList.append( { 'device': 'Voltronic',
                            'metric': 'SWDINPV.VIC.Mode.instMag[MX]',
                            'value' :  self.desiredInverterMode } )

     # Request Input Current Limit Change
     if self.desiredInputCurrentLimit != self.currentInputCurrentLimit :
       #print "Setting current limit to {0} from {1}".format( self.desiredInputCurrentLimit, self.currentInputCurrentLimit )
       actionList.append( { 'device': 'Voltronic',
                            'metric': 'SWDINPV.VIC.InpCurLim.instMag[MX]',
                            'value' :  self.desiredInputCurrentLimit } )

     # Publish Our Current State
     self.stateStore.putVal('SWIDINPV.NccNo2Controller.State', self.currentState)

     # Publish Action List
     self.stateStore.putVal('SWDINPV.ControllerActionList', actionList)

     # Debugging
     #print self.stateStore.printStateStore()
     #print "---  END  --- NCC Periodic Processing"

   #
   # Processing for normal grid feed mode
   #
   def stateGridFeed(self):
     vbatt = self.stateStore.getVal('SWDINPV.ZBAT1.Vol.instMag[MX]')

     # Desired Configuration For This State
     self.desiredInverterMode      = inverterModes.INVERTER
     self.desiredInputCurrentLimit = gridFeedCurrentLimit

     # Handle State Change
     if( self.currentBatteryVoltage > batLowerVoltLimit and \
         self.currentHour >= dischargeTimeStart and self.currentHour < dischargeTimeEnd ):
         # Use the battery to power peak loads for sportsground lights
         self.currentState = "Discharge"
     if( self.currentBatteryVoltage <= batLowerVoltLimit and \
         self.currentHour >= dischargeTimeStart and self.currentHour < dischargeTimeEnd ):
         # Run Out Of Battery, Revert to Grid Feed
         self.currentState = "Grid Feed"
     elif(self.currentHour >= chargeTimeStart and self.currentHour < chargeTimeEnd):
         # Charge The Battery At Nominated Times Of Day
         self.currentState = "Charge"

   #
   # Processing for charging battery
   #
   def stateCharging(self):
     # Desired Configuration For This State
     self.desiredInverterMode = inverterModes.INVCHRGR
     self.desiredInputCurrentLimit = gridFeedCurrentLimit

     # Handle State Change
     if(self.currentHour > chargeTimeEnd):
         self.currentState = "Grid Feed"

   #
   # Processing for dis-charging battery
   #
   def stateDisCharging(self):
     # Desired Configuration For This State
     self.desiredInverterMode = inverterModes.INVERTER
     self.desiredInputCurrentLimit = dischargeCurrentLimit

     # Handle State Change
     if(self.currentHour >= dischargeTimeEnd or self.currentBatteryVoltage < batLowerVoltLimit):
         self.currentState = "Grid Feed"
