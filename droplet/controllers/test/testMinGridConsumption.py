
import random
import math
import datetime
import numpy as np


# Controller
from controllers import minimiseGridConsumption

# State shared with controller
from stateStore import stateStore
myState = stateStore()

# Samples Per Hour
samplesHr     = 60.0*60.0
durationDays  = 1.0

# PV Characteristics
genPeakTime   = 13.0 # hr
genMag        = 5000.0 # W
genRand       = random.sample(xrange(30,100), int(durationDays))
genRand[:]    = [x / 100.0 for x in genRand]
# Ignore Randomisation
genRand[:]    = [1.0 for x in genRand]

# Load Characteristics
switchLoad     = 1000.0
baseLoadPeak   = 3000.0
baseLoadOffset = 500.0

# Battery Characteristics
minVBatt      = 47.6
maxVBatt      = 58.1
initialSoC    = 0.5    # Fraction of 1.0
initVBatt     = minVBatt + ((maxVBatt-minVBatt) * initialSoC)
batCapacity   = 9000.0 # Wh Useable

# Fake enums
def enum(**enums):
   return type('Enum', (), enums)

# Model of PV Generation
class pvModel(object):
    def __init__(self, peakCapacity, peakGenTime ):
        self.capacity = peakCapacity
        self.stateStore = stateStore()
        self.peakGenTime = peakGenTime
        self.peakCapacity = peakCapacity
    
    def update(self):
        time = self.stateStore.getVal('SWDINPV.time')
        simday = int( self.stateStore.getVal('SWDINPV.lintime') / 24.0)

        # Generator Power - AC Side
        x = time
        mu  = self.peakGenTime
        sig = 1.8
        self.gridPower =  np.exp(-np.power((x - mu)/(2.*sig), 2.)) * \
                          self.peakCapacity * genRand[simday]
            
        # Generator Voltage - AC Side
        self.gridVoltage = 240.0

        # Generator Current - AC Side
        self.gridCurrent = self.gridPower / self.gridVoltage

        # Publish Results
        self.stateStore.putVal('SWDINPV.SMATP.MMXN3.TotW.instMag[MX]', self.gridPower)
        self.stateStore.putVal('SWDINPV.VIC.MMXN3.TotW.instMag[MX]', self.gridPower)

        self.stateStore.putVal('SWDINPV.SMATP.MMXN3.Vol.instMag[MX]', self.gridVoltage)
        self.stateStore.putVal('SWDINPV.VIC.MMXN3.Vol.instMag[MX]', self.gridVoltage)

        self.stateStore.putVal('SWDINPV.SMATP.MMXN3.Cur.instMag[MX]', self.gridCurrent )
        self.stateStore.putVal('SWDINPV.VIC.MMXN3.Cur.instMag[MX]', self.gridCurrent )

# Inverter Modes
inverterModes = enum(CHARGER=1, INVERTER=2, INVCHRGR=3, OFF=4)

class inverterModel( object ):

    def __init__(self):
        #
        self.stateStore = stateStore()

        # Startup in Off Mode
        self.stateStore.putVal('SWDINPV.VIC.Mode.instMag[MX]', inverterModes.OFF )

        # Startup Assuming Transistion Immediately Into GridFeed
        # Unilimited Grid Current, No Charger Current
        self.stateStore.putVal('SWDINPV.VIC.InpCurLim.instMag[MX]', 100.0)
        self.stateStore.putVal('SWDINPV.VIC.ChgCurLim.instMag[MX]', 0.0)

    def update( self ):
        # Transistion System Mode
        self.currentMode = self.stateStore.getVal('SWDINPV.CMINC.InvChgrMode.SetPoint')
        self.stateStore.putVal('SWDINPV.VIC.Mode.instMag[MX]', self.currentMode )

        # Transition Input Current Limit
        self.inputCurrentLimit = self.stateStore.getVal('SWDINPV.CMINC.InpCurLim.SetPoint')
        self.stateStore.putVal('SWDINPV.VIC.InpCurLim.instMag[MX]', self.inputCurrentLimit)

        # Transition Charger Current Limit 
        self.chgrCurrentLimit = self.stateStore.getVal('SWDINPV.CMINC.ChgCurLim.SetPoint')
        self.stateStore.putVal('SWDINPV.VIC.ChgCurLim.instMag[MX]', self.chgrCurrentLimit)

        # Inverter Input Voltage
        self.stateStore.putVal('SWDINPV.VIC.MMXN3.Vol.instMag[MX]', 230.0)

# Model of Battery
class batteryModel(object):
    def __init__(self, initialSoC, capacity, minV, maxV ):
        self.stateStore = stateStore()

        self.SoC = initialSoC
        self.WhCapacity = capacity
        self.storedEnergy = initialSoC * capacity 

        self.minV = minV
        self.maxV = maxV

        # Assume Voltage Linear With SoC --- Not True ... but good enough
        # for now.
        self.V = (self.SoC * (self.maxV - self.minV)) + self.minV


    def update( self ):

        # Current Mode Of The Inverter
        self.inverterMode = self.stateStore.getVal('SWDINPV.VIC.Mode.instMag[MX]')

        # Available Input Power Given Input Current Limit
        self.inCurLim = self.stateStore.getVal('SWDINPV.VIC.InpCurLim.instMag[MX]')
        self.inVolt   = self.stateStore.getVal('SWDINPV.VIC.MMXN3.Vol.instMag[MX]')
        self.inPower  = self.inCurLim * self.inVolt

        # Load Power 
        self.load     = self.stateStore.getVal('SWDINPV.VIC.MMXN2.TotW.instMag[MX]')

        # Net Power May Be Supplied To/From The Battery
        self.avail    = self.inPower  - self.load

        # Charger Power Limit
        self.chgCurLim = self.stateStore.getVal('SWDINPV.VIC.ChgCurLim.instMag[MX]')
        self.chgPower  = self.V * self.chgCurLim

        # Charging Power 
        self.chgPower = min(self.chgPower, self.avail)
            
        # Is the charger on?
        if self.inverterMode == inverterModes.INVERTER:
            self.chgPower = min( 0.0, self.chgPower )
        elif self.inverterMode == inverterModes.OFF:
            self.chgPower = 0.0

        # Resulting Battery Stored Energy
        self.storedEnergy = self.storedEnergy + (self.chgPower / samplesHr)
        self.storedEnergy = min(self.storedEnergy, self.WhCapacity)

        # Resulting SoC
        self.SoC = self.storedEnergy / self.WhCapacity

        # Resulting Voltage
        self.V = (self.SoC * (self.maxV - self.minV)) + self.minV
        self.V = min( self.V, self.maxV )
        self.V = max( self.V, self.minV )

        # Publish Results
        self.stateStore.putVal('SWDINPV.VIC.ZBAT1.SoC.instMag[MX]', self.SoC)
        self.stateStore.putVal('SWDINPV.VIC.ZBAT1.Vol.instMag[MX]', self.V)
        self.stateStore.putVal('SWDINPV.VIC.ZBAT1.StoredEnenergy.instMag[MX]', self.storedEnergy)


# Model of Load
class loadModel( object ):
    def __init__(self, switchLoadMag, baseLoadMag, baseLoadOffset):
        self.stateStore = stateStore()
        self.switchLoadMag = switchLoadMag
        self.baseLoadMag = baseLoadMag
        self.baseLoadOffset = baseLoadOffset

    def updateLoad( self ):
        time = self.stateStore.getVal('SWDINPV.time')
        timeOdd = int(time) % 2

        # Base Load ... Double Bell at 9am and 1530
        x = time; mu  = 9.0; sig = 1.5
        peak1 = np.exp(-np.power((x - mu)/(2.*sig), 2.)) * self.baseLoadMag
        x = time; mu  = 15.5; sig = 1.5
        peak2 = np.exp(-np.power((x - mu)/(2.*sig), 2.)) * self.baseLoadMag
        baseLoad = max(peak1, peak2) + self.baseLoadOffset

        # Incoporate Switching Load

        if timeOdd == 0:
            switchLoad = self.switchLoadMag 
        else:
            switchLoad = 0

        totalLoad = switchLoad + baseLoad

        self.stateStore.putVal('SWDINPV.VIC.MMXN2.TotW.instMag[MX]', totalLoad)


# Create Models
pvModel  = pvModel( genMag, genPeakTime )
battery  = batteryModel( initialSoC, batCapacity, minVBatt, maxVBatt )
load     = loadModel( switchLoad, baseLoadPeak, baseLoadOffset )
inverter = inverterModel( )

# Create Controller
controller = minimiseGridConsumption.minimiseGridConsumption({})

# Results
import copy
results = []

# Simulation Loop
for timeIdx in range( 0, int(24*durationDays*samplesHr)):
    # Time Of Day
    time    = (timeIdx / samplesHr) % 24.0
    myState.putVal('SWDINPV.time', time)
    lintime = (timeIdx / samplesHr) 
    myState.putVal('SWDINPV.lintime', lintime)

    # Datetime Object
    dayOffset = datetime.timedelta( hours=int(time), minutes=(time-int(time))*60 )
    day = datetime.datetime( 2016, 4, 8 )
    curTime = day + dayOffset
    myState.putVal('SWDINPV.SystemTime', curTime)

    # System Mode
    systemMode = myState.getVal('SWDINPV.VIC.Mode.instMag[MX]')

    # Generation
    pvModel.update()

    # Battery
    battery.update()

    # Load
    load.updateLoad()

    # Inverter
    inverter.update()

    # Controller
    controller.periodicProcessing()

    # Save Results
    res = copy.deepcopy(myState.__dict__)
    results.append(res)

    # Debug
    #myState.printStateStoreCsv()
    #myState.printStateStore()
    #print results

#
# Analysis
#
import pandas as pd
import pylab as plt

resultFrame = pd.DataFrame.from_dict( results )

# System Mode
fig, axes = plt.subplots(nrows=4, ncols=1, sharex=True)
resultFrame['SWDINPV.CMINC.StateId'].astype(float).plot( label='Controller', legend=True, ax=axes[0] )
resultFrame ['SWDINPV.VIC.Mode.instMag[MX]'].astype(float).plot( label='Charger', legend=True, ax=axes[0] )
axes[0].grid()

resultFrame['SWDINPV.SMATP.MMXN3.TotW.instMag[MX]'].astype(float).plot(label='Gen',
                                                                       ax=axes[1], 
                                                                       legend=True )

resultFrame['SWDINPV.VIC.MMXN2.TotW.instMag[MX]'].astype(float).plot ( label='Con',
                                                                       ax=axes[1], 
                                                                       legend=True )
axes[1].grid()

resultFrame['SWDINPV.VIC.ZBAT1.Vol.instMag[MX]'].astype(float).plot ( label='Bat Vol', 
                                                                      legend=True,
                                                                      secondary_y=True,
                                                                      ax=axes[2] )

resultFrame['SWDINPV.VIC.ZBAT1.SoC.instMag[MX]'].astype(float).plot ( label='Bat SoC',
                                                                      ax=axes[2],
                                                                      legend=True)
axes[2].grid()


resultFrame['SWDINPV.VIC.InpCurLim.instMag[MX]'].astype(float).plot ( label='GridCurLim', 
                                                                      legend=True,
                                                                      secondary_y=True,
                                                                      ax=axes[3] )
resultFrame['SWDINPV.VIC.ChgCurLim.instMag[MX]'].astype(float).plot ( label='ChgCurLim',
                                                                      ax=axes[3],
                                                                      legend=True)
axes[3].grid()


plt.show()

#battV.plot(ax=axes[0], legend=False)
#modeV.plot(ax=axes[1], legend=False)
#iclV.plot(ax=axes[2], legend=False)
#axes[2].grid()


