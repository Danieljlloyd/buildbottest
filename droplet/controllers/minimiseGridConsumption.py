
from controllerBase import BaseController
from controllerBase import enum

#
# Use Case:
#
# This controller is to be used when the system is battery backed, and has
# a PV inverter, Inverter/Charger with Battery Support, and a load.
#
# The objective of the controller is to maximise storage from PV generation
# and minimise grid consumption. The battery will only be charged from the PV.
#

# Inverter Modes
inverterModes = enum(CHARGER=1, INVERTER=2, INVCHRGR=3, OFF=4)

###############################################################################
# Controller Context Class
###############################################################################
class minimiseGridConsumption( BaseController ):

   # Class Constructor
   def __init__( self, config ):
      super(minimiseGridConsumption, self).__init__( )

      self.config = config

      # Configuration Variables. Will go into JSON File
      self.config['batDischargeEntryThreshold'] = 0.35 # Percent SoC
      self.config['batDischargeExitThreshold']  = 0.20 # Percent SoC
      self.config['maxInputCurrentLimit']       = 50.0 # Amps
      self.config['minInputCurrentLimit']       = 10.5 # Amps
      self.config['maxChargerCurrentLimit']     = 50.0 # Amps
      self.config['minChargerCurrentLimit']     = 10.5 # Amps

      # keep track of number of iterations
      self.iterCtr = 0
      # We average values over a minute for control actions.
      self.controlPeriod = 60

      # State Handlers
      self.stateExcessConsumption = sExcessConsumption( config )
      self.stateExcessGeneration  = sExcessGeneration( config )
      self.stateGridFeed          = sGridFeed( config )

      # Initial State
      self.currentState = self.stateGridFeed
      self.nextState    = self.stateGridFeed
      self.currentState.enterState()

      # A matrix to capture raw Samples in for averaging
      self.Samples = [[0 for x in range(self.controlPeriod)] for x in range(7)]

      # Initialised Internal State Measurments
      self.calcState()

      # Initialise Averages To Current Value
      for x in range(self.controlPeriod):
          self.Samples[0][x] = self.generatedPower
          self.Samples[1][x] = self.inputPower
          self.Samples[2][x] = self.loadPower
          self.Samples[3][x] = self.chargerPower
          self.Samples[4][x] = self.exportEstimate
          self.Samples[5][x] = self.inputVoltage
          self.Samples[6][x] = self.loadVoltage

      # Reload State to Recalculate Averages
      self.calcState()

   # Identifier for Config File
   @staticmethod
   def implements(identifier):
      return "minimiseGridConsumption" in identifier

   # Load State Variables
   def calcState( self ):
      # System Time
      self.systemTime = self.stateStore.getVal('SWDINPV.SystemTime')
      # Quantity of Generated Current (by PV system)
      self.generatedPower    = self.stateStore.getVal('SWDINPV.SMATP.MMXN3.TotW.instMag[MX]')
      self.generatedCurrent  = self.stateStore.getVal('SWDINPV.SMATP.MMXN3.Cur.instMag[MX]')
      # Averaged Measurements
      self.inputPower      = self.stateStore.getVal('SWDINPV.VIC.MMXN3.TotW.instMag[MX]')
      self.loadPower       = self.stateStore.getVal('SWDINPV.VIC.MMXN2.TotW.instMag[MX]')
      self.chargerPower    = self.stateStore.getVal('SWDINPV.VIC.ZBAT1.TotW.instMag[MX]')
      self.inputVoltage    = self.stateStore.getVal('SWDINPV.VIC.MMXN3.Vol.instMag[MX]')
      self.loadVoltage     = self.stateStore.getVal('SWDINPV.VIC.MMXN2.Vol.instMag[MX]')
      # Instantaneous Measurements
      self.batterySoC      = self.stateStore.getVal('SWDINPV.VIC.ZBAT1.SoC.instMag[MX]')
      self.batteryV        = self.stateStore.getVal('SWDINPV.VIC.ZBAT1.Vol.instMag[MX]')
      # estimates
      self.exportEstimate  = self.generatedPower - self.inputPower

      # contol mechanisms
      self.inputCurrentLimit   = self.stateStore.getVal('SWDINPV.VIC.InpCurLim.instMag[MX]')
      self.chargerCurrentLimit = self.stateStore.getVal('SWDINPV.VIC.ChgCurLim.instMag[MX]')
      self.systemMode          = self.stateStore.getVal('SWDINPV.VIC.Mode.instMag[MX]')
      # initialise target control Values To Avoid Unwanted Control Actions
      self.targetInputCurrentLimit   = self.inputCurrentLimit
      self.targetChargerCurrentLimit = self.chargerCurrentLimit

      # update sample matrix
      self.Samples[0][self.iterCtr] = self.generatedPower
      self.Samples[1][self.iterCtr] = self.inputPower
      self.Samples[2][self.iterCtr] = self.loadPower
      self.Samples[3][self.iterCtr] = self.chargerPower
      self.Samples[4][self.iterCtr] = self.exportEstimate
      self.Samples[5][self.iterCtr] = self.inputVoltage
      self.Samples[6][self.iterCtr] = self.loadVoltage

      # calculate averages
      self.meanGeneratedPower = self.meanOf(self.Samples[0])
      self.meanInputPower     = self.meanOf(self.Samples[1])
      self.meanLoadPower      = self.meanOf(self.Samples[2])
      self.meanChargerPower   = self.meanOf(self.Samples[3])
      self.meanExportEstimate = self.meanOf(self.Samples[4])
      self.meanInputVoltage   = self.meanOf(self.Samples[5])
      self.meanLoadVoltage    = self.meanOf(self.Samples[6])

      # publish averages
      self.stateStore.putVal( 'SWDINPV.CMINC.meanGenPower',       self.meanGeneratedPower )
      self.stateStore.putVal( 'SWDINPV.CMINC.meanInputPower',     self.meanInputPower)
      self.stateStore.putVal( 'SWDINPV.CMINC.meanLoadPower',      self.meanLoadPower)
      self.stateStore.putVal( 'SWDINPV.CMINC.meanChargerPower',   self.meanChargerPower)
      self.stateStore.putVal( 'SWDINPV.CMINC.meanExportEstimate', self.meanExportEstimate)
      self.stateStore.putVal( 'SWDINPV.CMINC.meanInputValue',     self.meanInputVoltage)
      self.stateStore.putVal( 'SWDINPV.CMINC.meanLoadVoltage',    self.meanLoadVoltage)


   def meanOf( self, vector ):
      return sum(vector) / len(vector)

   # Periodic Processing
   def periodicProcessing( self ):
      # Count An Iteration
      self.iterCtr = (self.iterCtr + 1) % self.controlPeriod

      # Update State Variables
      self.calcState()

      #  Check State Transistions
      self.checkStateTransitions()

      # Perform State Transition Processing
      if self.nextState != self.currentState:
          self.currentState.exitState()
          self.nextState.enterState()
          self.currentState = self.nextState

      # Do State Calculations
      self.currentState.periodicProcessing()

      # Perform Control Actions
      self.doControlActions()

   # Build Control Action Messages For droplet to action
   def doControlActions( self ):
       pass

   #
   # Determine if a state change is required
   #
   def checkStateTransitions( self ):

      # Generating More Than Consuming. Store Excess.
      if self.meanGeneratedPower > self.meanLoadPower:
         self.nextState = self.stateExcessGeneration

      # Consuming More Than Generating, Stored Energy Available
      elif self.batterySoC > self.config['batDischargeEntryThreshold'] :
         self.nextState = self.stateExcessConsumption

      # Consuming More Than Generating, No Stored Energy 
      elif self.batterySoC <= self.config['batDischargeExitThreshold'] :
         self.nextState = self.stateGridFeed

###############################################################################
# State Handling
###############################################################################

from stateBase import stateBase

class sExcessConsumption( stateBase ):
    # Constructor
    def __init__(self, config) :
        super(sExcessConsumption, self).__init__( config )

    # State Entry Processing
    def enterState(self):
        self.stateStore.putVal('SWDINPV.CMINC.State', 'ExcessConsumption')
        self.stateStore.putVal('SWDINPV.CMINC.StateId', '0')

    # Draw Down On The Battery
    def periodicProcessing(self):
        # Turn Charger Off
        self.stateStore.putVal('SWDINPV.CMINC.InvChgrMode.SetPoint', inverterModes.INVERTER )
        self.targetChargerCurrentLimit = self.config['minChargerCurrentLimit']
        # Feed From Battery Where Possible
        self.targetInputCurrentLimit   = self.config['minInputCurrentLimit']
        # Pass Back Control Decisions / Setpoints
        self.stateStore.putVal('SWDINPV.CMINC.ChgCurLim.SetPoint', self.targetChargerCurrentLimit )
        self.stateStore.putVal('SWDINPV.CMINC.InpCurLim.SetPoint', self.targetInputCurrentLimit )

class sExcessGeneration( stateBase ):
    # Constructor
    def __init__(self, config) :
        super(sExcessGeneration, self).__init__( config )

    # State Entry Processing
    def enterState(self):
        self.stateStore.putVal('SWDINPV.CMINC.State', 'ExcessGeneration')
        self.stateStore.putVal('SWDINPV.CMINC.StateId', '1')

    # Store Excess PV In The Battery
    def periodicProcessing(self):
        # Turn Charger On
        self.stateStore.putVal('SWDINPV.CMINC.InvChgrMode.SetPoint', inverterModes.INVCHRGR )
        # Set The Input Currnet To The Output Current Of the PV Inverter
        self.targetInputCurrentLimit   = self.config['maxInputCurrentLimit']
        # Calculate Residual Power 
        self.meanGeneratedPower = self.stateStore.getVal('SWDINPV.CMINC.meanGenPower')
        self.meanLoadPower      = self.stateStore.getVal('SWDINPV.CMINC.meanLoadPower')
        self.residualPower      = self.meanGeneratedPower - self.meanLoadPower
        # Put Residual Power In The Battery
        self.batteryV = self.stateStore.getVal('SWDINPV.VIC.ZBAT1.Vol.instMag[MX]')
        self.targetChargerCurrentLimit = self.residualPower / self.batteryV
        self.targetChargerCurrentLimit = min(self.targetChargerCurrentLimit, self.config['maxChargerCurrentLimit'])
        self.targetChargerCurrentLimit = max(self.targetChargerCurrentLimit, self.config['minChargerCurrentLimit'])
        # Pass Back Control Decisions / Setpoints
        self.stateStore.putVal('SWDINPV.CMINC.ChgCurLim.SetPoint', self.targetChargerCurrentLimit )
        self.stateStore.putVal('SWDINPV.CMINC.InpCurLim.SetPoint', self.targetInputCurrentLimit )

class sGridFeed( stateBase ):
    # Constructor
    def __init__(self, config) :
        super(sGridFeed, self).__init__( config )

    # State Entry Processing
    def enterState(self):
        self.stateStore.putVal('SWDINPV.CMINC.State', 'GridFeed')
        self.stateStore.putVal('SWDINPV.CMINC.StateId', '2')

    # Turn Of Charger, Feed Site From Grid
    def periodicProcessing(self):
        # Turn Charger Off
        self.stateStore.putVal('SWDINPV.CMINC.InvChgrMode.SetPoint', inverterModes.INVERTER )
        self.targetChargerCurrentLimit = self.config['minChargerCurrentLimit']
        # Turn On The Grid Feed
        self.targetInputCurrentLimit   = self.config['maxInputCurrentLimit']
        # Pass Back Control Decisions / Setpoints
        self.stateStore.putVal('SWDINPV.CMINC.ChgCurLim.SetPoint', self.targetChargerCurrentLimit )
        self.stateStore.putVal('SWDINPV.CMINC.InpCurLim.SetPoint', self.targetInputCurrentLimit )



