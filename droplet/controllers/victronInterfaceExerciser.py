from controllerBase import BaseController
from controllerBase import enum

#
# Note: This is a test object only. It's purpose is to control a Victron device via
#       ModbuTCP over the range of available inputs and input ranges.
#       It is not to be used in production situations for control of an inverter.
#
#       Victron inverters have variable control limits in terms of input current 
#       and system state controls, as follows:
#
#       1. Input Current Limit - There is a minimum value for the input current limit
#          dependant upon the type and size of the inverter / charger.
#
#       2. Charger Current Limit - The current limit applied to the battery charger
#
#       3. System Mode (Front Panel Switch) - may be controlled from the ModbusTCP
#          interface into one of INVERTER only, CHARGER only, INVERTER / CHARGER (on), and OFF.
#
#       4. Battery Charger Mode (TO BE CONFIRMED) - into Bulk, Adsorption, and Float by
#          toggling control of the front panel switch through a particular sequence.
#
#       This controller may be loaded into the droplet and will run through each mode and
#       input / output range in sequnce.
#

inverterModes = enum(CHARGER=1, INVERTER=2, INVCHRGR=3, OFF=4)

# Base Controller Class
class victronControlExerciser( BaseController ):
    def __init__(self, config={}):
        super(victronControlExerciser, self).__init__( )

        self.config = config
        self.iterCtr = 0
        self.iterPeriod = 30.0 # iterations

        self.inverterState = inverterModes.INVCHRGR
    
        self.inputCurrentLimitMin = 0.0
        self.inputCurrentLimitMax = 100.0
        self.inputCurrentLimitInc = (self.inputCurrentLimitMax - self.inputCurrentLimitMin) / self.iterPeriod
        self.inputCurrentLimitTarg = self.inputCurrentLimitMin
    
        self.chargerCurrentLimitMin = 0.0
        self.chargerCurrentLimitMax = 100.0
        self.chargerCurrentLimitInc = (self.chargerCurrentLimitMax - self.chargerCurrentLimitMin) / self.iterPeriod
        self.chargerCurrentLimitTarg = self.chargerCurrentLimitMin

    def __del__(self):
        pass

    @staticmethod
    def implements( identifier ):
        return "victronControlExerciser" in identifier
      
    def periodicProcessing(self):

        self.iterCtr = (self.iterCtr + 1 ) % self.iterPeriod
        #
        # New Setpoints
        #
        self.targetInputCurrentLimit = self.iterCtr * self.inputCurrentLimitInc
        self.stateStore.putVal('SWDINPV.CMINC.InpCurLim.SetPoint', self.targetInputCurrentLimit )
        self.targetChargerCurrentLimit = self.iterCtr * self.chargerCurrentLimitInc
        self.stateStore.putVal('SWDINPV.CMINC.ChgCurLim.SetPoint', self.targetChargerCurrentLimit )

        # Change Inverter Modes every self.iterPeriod
        if self.iterCtr == 0:
            self.inverterState = self.inverterState + 1 

        if self.inverterState == 5:
            self.inverterState = inverterModes.CHARGER

        self.stateStore.putVal('SWDINPV.CMINC.InvChgrMode.SetPoint', self.inverterState )

        # Build Commands
        controls = []
        controls.append( {'metric': 'SWDINPV.VIC.Mode', 'value': self.inverterState} )
        controls.append( {'metric': 'SWDINPV.VIC.AICL', 'value': self.targetInputCurrentLimit} )
        #controls.append( {'metric': 'SWDINPV.VIC.CCL', 'value': self.targetChargerCurrentLimit} )

        return controls



