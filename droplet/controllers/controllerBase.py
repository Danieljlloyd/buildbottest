from types import TypeType
from stateStore import stateStore

# Fake enums
def enum(**enums):
   return type('Enum', (), enums)

# Base Controller Class
class BaseController(object):
   def __init__(self, config={}):
      self.stateStore = stateStore()
      self.config = config
      pass

   def __del__(self):
      pass

   def implements( identifier ):
      return False
      
   def periodicProcessing(self):
      pass

# Controller Factory
class ControllerFactory(object):
    @staticmethod
    def newController(identifier, config={}):
        # Walk through all Controller classes
        controllerClasses = BaseController.__subclasses__()
        for controllerClass in controllerClasses:
            if controllerClass.implements(identifier):
                return controllerClass(config)
        # if research was unsuccessful, raise an error
        raise ValueError('No controller supporting "%s".' % identifier)
