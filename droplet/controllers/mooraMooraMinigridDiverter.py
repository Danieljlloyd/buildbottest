
from controllerBase import BaseController
from controllerBase import enum

from datetime import datetime, timedelta

#
# Use Case:
#
# Determine the routing of energy at Moora Moora as per Glen Morris specification below:
#
# Relay 1 On at 51Hz; after 10mins check if frequency is 50Hz or less, 
# if true turn off. (Load Dump "Bob's House")
#
# Relay 2 On at 51.2Hz; after 10mins check if frequency is 50Hz or less, 
# if true turn off. (Load Dump "Jo's House")
#
# Relay 3 If generator is running (SP Pro can provide digital signal) turn on 
# (use spare generator capacity to run "Bob" and "Jo's" House dump loads)
#
# Relay 4 If frequency is > 51.5Hz turn on for 10mins; check if still above 51.5Hz 
# (turn on 7.2kW three phase hotwater element to dampen excess power on microgrid)
#

###############################################################################
# Controller Context Class
###############################################################################
class mooraMooraEnergyDiverter( BaseController ):

   # Class Constructor
   def __init__( self, config ):
      super(mooraMooraEnergyDiverter, self).__init__( )
      self.config = config

      # Latch Times
      self.relay1LatchTime = None
      self.relay2LatchTime = None
      self.relay3LatchTime = None
      self.relay4LatchTime = None

   # Identifier for Config File
   @staticmethod
   def implements(identifier):
      return "mooraMooraEnergyDiverter" in identifier

   # Periodic Processing
   def periodicProcessing( self ):
      # Determine Current Mains / Grid Frequency
      # For Glenn Morris .. this is the AC Frequency Measured By The Conext XW+ Inverter
      self.currentFreq = self.stateStore.getVal('SWDINPV.MMXN4.Freq.instMag[MX]')

      # Dump To Bobs House For Monimum of 10 mionutes If Freq > 51.0 
      if self.relay1LatchTime is None and self.currentFreq >= 51.0:
        self.relay1LatchTime = datetime.now()
        self.stateStore.putVal('SWDINPV.DAERB.Relay1.Control', True)
      elif self.relay1LatchTime is not None:
	timeSinceLatch = datetime.now() - self.relay1LatchTime
	if timeSinceLatch >= timedelta(minutes=10) and self.currentFreq < 51.0:
          self.relay1LatchTime = None
          self.stateStore.putVal('SWDINPV.DAERB.Relay1.Control', False)
      
      # Load Dump Jos House
      if self.relay2LatchTime is None and self.currentFreq >= 51.2:
        self.relay2LatchTime = datetime.now()
        self.stateStore.putVal('SWDINPV.DAERB.Relay2.Control', True)
      elif self.relay2LatchTime is not None:
	timeSinceLatch = datetime.now() - self.relay2LatchTime
	if timeSinceLatch >= timedelta(minutes=10) and self.currentFreq < 51.2:
          self.relay2LatchTime = None
          self.stateStore.putVal('SWDINPV.DAERB.Relay2.Control', False)
 
      # Generator Is On
      if (self.currentFreq <= 49.5) and (self.currentFreq > 40.0):
        self.stateStore.putVal('SWDINPV.DAERB.Relay3.Control', True)
      else:
        self.stateStore.putVal('SWDINPV.DAERB.Relay3.Control', False)

      # Load Dump To Hotwater Elements
      if self.relay4LatchTime is None and self.currentFreq >= 51.5:
        self.relay4LatchTime = datetime.now()
        self.stateStore.putVal('SWDINPV.DAERB.Relay4.Control', True)
      elif self.relay4LatchTime is not None:
	timeSinceLatch = datetime.now() - self.relay4LatchTime
	if timeSinceLatch >= timedelta(minutes=10) and self.currentFreq < 51.5:
          self.relay4LatchTime = None
          self.stateStore.putVal('SWDINPV.DAERB.Relay4.Control', False)
 
      return []

   # Build Control Action Messages For droplet to action
   def doControlActions( self ):
       return []

