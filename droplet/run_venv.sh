#!/bin/bash

# There's a dependency that it's assumed there is a virtualenv installed
# at ~/venv/blast/
source $HOME/venv/droplet/bin/activate
exec python droplet.py --broker mqtt.switchd.in

