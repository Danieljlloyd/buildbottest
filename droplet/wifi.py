import subprocess

class Wifi(object):
    """Utility functions for configuring wicd"""
    
    def __init__(self):
        pass
        
    def list_wireless_networks(self):
        networks = []
        output = subprocess.check_output('wicd-cli --wireless --scan --list-networks'.split())
        
        lines = output.split('\n')[1:] # Skip header line
        for line in lines:
            network = {}
            items = line.split('\t')
            if len(items) < 4: continue            
            
            network['number'] = items[0]
            network['bssid'] = items[1]
            network['channel'] = items[2]
            network['essid'] = items[3]
            
            networks.append(network)
        
        return networks
          
    def network_details(self, network_number):
        details = {}
        
        command = 'wicd-cli --wireless --network {0} --network-details'.format(network_number)
        
        output = subprocess.check_output(command.split())
        lines = output.split('\n')
        
        for line in lines:
            if len(line) == 0: continue
            
            name, value = line.split(':', 1)
            details[name.lower()] = value.strip()
        
        return details
            
    def connect(self, essid, password):
        # Get the wicd network number
        networks = self.list_wireless_networks()
        network_num = -1
        for network in networks:
            if network['essid'].lower() == essid.lower():
                network_num = network['number']   
                break

        if network_num == -1:
            raise Exception("Network '{0}' is not in range".format(essid))                
                
        # Figure out the encryption type        
        network_info = self.network_details(network_num)
        using_encryption = network_info.get('encryption', '') == "On"
        encryption_type = network_info.get('encryption method', '')
        
        # Start configuring wicd
        if encryption_type in ["WPA", "WPA2"]:
            # Assume wpa-psk
            command = 'wicd-cli --wireless --network {0} --network-property enctype --set-to wpa-psk' \
                .format(network_num)
            subprocess.check_call(command.split())
                 
            command = 'wicd-cli --wireless --network {0} --network-property apsk --set-to {1}' \
                .format(network_num, password)
            subprocess.check_call(command.split())

        command = 'wicd-cli --wireless --network {0} --save'.format(network_num)
        subprocess.check_call(command.split())
        
        command = 'wicd-cli --wireless --network {0} --connect'.format(network_num)
        subprocess.check_call(command.split())
          
if __name__ == '__main__':  
    w = Wifi()
    networks = w.list_wireless_networks()
    print networks
    
    details = w.network_details(0)
    print details