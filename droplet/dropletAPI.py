from SimpleXMLRPCServer import SimpleXMLRPCServer, \
                               SimpleXMLRPCRequestHandler
import threading

"""
API to the droplet state store
"""
class DropletAPI(object):
    def __init__(self, aStateStore):
        self.stateStore = aStateStore

    def getVal(self, aKey):
        return self.stateStore.getVal(aKey)

"""
Request handler for XML-RPC APIs
"""
class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_paths = ( '/RPC2' )

"""
Droplet API server thread
"""
class DropletAPIThread(threading.Thread):
    def __init__(self, aStateStore, addr):
        self.stateStore = aStateStore
        threading.Thread.__init__(self)

        # Create Droplet API
        self.dropletAPI = DropletAPI(self.stateStore)

        # Start the XML-RPC server
        self.server = SimpleXMLRPCServer( (addr['ip'], addr['port']), 
                                     requestHandler = RequestHandler )
        self.server.register_introspection_functions()

        # Register Droplet API instance
        self.server.register_instance(self.dropletAPI)

    def run(self):
        self.server.serve_forever()
