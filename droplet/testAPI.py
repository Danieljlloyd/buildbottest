import xmlrpclib
import time

server = xmlrpclib.ServerProxy('http://localhost:8000')

state = { 'SWDINplant.STMP1.Tmp.instMag[MX]': None, 
          'SWDINPV.MMXN3.TotW.instMag[MX]': None,
          'SWDINPV.MMXN1.TotW.instMag[MX]': None,
          'SWDINPV.ZBAT1.Vol.instMag[MX]': None,
          'time': None                                }

while True:
    # Read form Droplet API
    for key in state.keys():
        state[key] = server.getVal(key)

    # Print results
    print "--- API Return Values"
    for key, value in state.iteritems():
        print "{}: {}".format(key, value)

    time.sleep(1)
