#!/usr/bin/python

import mosquitto
import os
import time
from datetime import datetime
import random
import uuid

from Measurements_pb2 import MeasurementSnapshot, Measurement, Quality 
    
class SimulatedDevice:
  def __init__(self, identifier, host):
    # Config
    self.uuid_str = identifier
    self.mqtt_broker_url = host
    self.mqtt_broker_port = 1883
    
    self.waiting_loop = True

    self.mqtt_connection = None
    self.connect_to_broker()
    
  def connect_to_broker(self):
    pid = os.getpid()
    unique_client_id = "blast_" + str(pid)# + "_" + self.uuid_str
    
    self.mqtt_connection = mosquitto.Mosquitto(unique_client_id, userdata=self)
    self.mqtt_connection.connect(self.mqtt_broker_url, self.mqtt_broker_port, 60, True)    
    self.mqtt_connection.on_message = SimulatedDevice.on_message
    self.mqtt_connection.subscribe("pong/{0}".format(self.uuid_str), 0)
    
  @staticmethod
  def on_message(mosq, self, msg):
    self.handle_pong(msg)      

  def current_unix_time(self):
    now = datetime.now()
    return time.mktime(now.timetuple()) + (now.microsecond / 1e6)

  def handle_pong(self, msg):
    rec_time = self.current_unix_time()
    snapshot = MeasurementSnapshot()
    snapshot.ParseFromString(msg.payload)

    for m in snapshot.measurements:
      if m.name == "time:start":
        sent_time = m.double_val
    
    # Add received time
    measurement = snapshot.measurements.add()
    measurement.double_val = rec_time
    measurement.point_uuid.value = self.uuid_str
    measurement.name = "time:stop"
    measurement.type = Measurement.DOUBLE
    measurement.quality.validity = Quality.GOOD   
    measurement.time = int(measurement.double_val)
    measurement.is_device_time = True
    
    # Post back to be logged
    topic = "measurements"
    msg = snapshot.SerializeToString()
    self.mqtt_connection.publish(topic, msg)
   
    self.waiting_loop = False
    
    print "Received Pong. Time = {0:.0f}ms".format((rec_time-sent_time)*1000)
    
  def post_ping(self):
    """ Post a single measurement to the message broker. """
    topic = "measurements"
    
    snapshot = MeasurementSnapshot()
    measurement = snapshot.measurements.add()
    measurement.point_uuid.value = self.uuid_str
    measurement.name = 'time:start'
    measurement.type = Measurement.DOUBLE
    measurement.quality.validity = Quality.GOOD   
    measurement.time = int(self.current_unix_time())
    measurement.is_device_time = True
    measurement.double_val = self.current_unix_time()
    
    msg = snapshot.SerializeToString()
    print "Ping sent" 

    self.mqtt_connection.publish(topic, msg)
    self.mqtt_connection.loop(0)
    
def main(args):
  import sys
  import argparse

  parser = argparse.ArgumentParser(description='Simulate a simple solar installation.')
  parser.add_argument('-b', '--broker', default='localhost',
                     help='MQTT broker to connect to (default: localhost)')
  parser.add_argument('-d', '--device',
                     help='Device ID (default: generate UUID from MAC address)')
  parser.add_argument('-w', '--wait', default=1,
                     help='Delay between pings in seconds (default: 1 second)')
  parser.add_argument('-r', '--repeat', default=10,
                     help='Number of times to repeat (default: 10)')
  parser.add_argument('-l', '--loop', action='store_true', default=False,
                     help='Loop forever (default: False)')

  _args = parser.parse_args()

  if _args.device:
    device_id = _args.device
  else:
    device_id = str(uuid.UUID(int=uuid.getnode()))

  mqtt_host = _args.broker

  wait = float(_args.wait)
  repeat = int(_args.repeat)
  loop = _args.loop

  print "Device ID: {0}".format(device_id)
  print "Broker:    {0}".format(mqtt_host)
  
  device = SimulatedDevice(device_id, mqtt_host)

  while True:
    for n in range(0, repeat):
      device.post_ping()
      device.waiting_loop = True

      while device.mqtt_connection.loop(0) == 0 and device.waiting_loop:
        pass

      # This loop is surprisingly needed - to flush the measurement message
      device.mqtt_connection.loop()
      time.sleep(wait)

    if loop == False:
      break
    

if __name__ == '__main__':
  import sys
  main(sys.argv[1:])    
  
